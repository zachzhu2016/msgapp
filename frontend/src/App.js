import React, { Component } from 'react'
import {
    useLocation,
    BrowserRouter,
    Route,
    Switch,
    Redirect
} from 'react-router-dom'
import './App.css'

import LoginPage from './Page/LoginPage'
import HomePage from './Page/HomePage'
import NewFeedPage from './Page/NewFeedPage'
import NewGroupPage from './Page/NewGroupPage'
import AccountPage from './Page/AccountPage'
import UserProfilePage from './Page/UserProfilePage'
import ExplorePage from './Page/ExplorePage'
import FeedPage from './Page/FeedPage'
import RegisterPage from './Page/RegisterPage'
import GroupProfilePage from './Page/GroupProfilePage'
import OrganizationPage from './Page/OrganizationPage'
//import ResponsiveContainer from './Skeleton'

import ResponsiveContainer from './ResponsiveContainer'
import util from './util'
import ScrollToTop from './Component/ScrollToTop'

const App = () => {
    return (
        <ErrorBoundary>
            <BrowserRouter>
                <ScrollToTop />
                <ResponsiveContainer>
                    <Route exact path={['/home', '/']} component={HomePage} />
                    <Route
                        exact
                        path={'/organization/:orgId/explore'}
                        component={ExplorePage}
                    />
                    <Route
                        exact
                        path={'/organization'}
                        component={OrganizationPage}
                    />
                    {/*   
                    <Route exact path="/register" component={RegisterPage} />
                    <Route exact path="/login" component={LoginPage} />
                    */}
                    <Route
                        exact
                        path="/user/:userId"
                        component={UserProfilePage}
                    />
                    <Route exact path="/feed/:itemId" component={FeedPage} />
                    <Route
                        exact
                        path="/group/:groupId"
                        component={GroupProfilePage}
                    />

                    {/* private routes */}
                    <PrivateRoute
                        exact
                        path="/account"
                        component={AccountPage}
                    />
                    <PrivateRoute
                        exact
                        path="/new-feed"
                        component={NewFeedPage}
                    />
                    <PrivateRoute
                        exact
                        path="/new-group"
                        component={NewGroupPage}
                    />
                </ResponsiveContainer>
            </BrowserRouter>
        </ErrorBoundary>
    )
}

function PrivateRoute(props) {
    const location = useLocation()
    if (!util.getObject('token') && props.path === location.pathname) {
        return <Redirect to="/" />
    }
    return <Route exact path={props.path} component={props.component} />
}

class ErrorBoundary extends Component {
    constructor(props) {
        super(props)
        this.state = { hasError: false }
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true }
    }

    componentDidCatch(error, errorInfo) {
        // You can also log the error to an error reporting service
        //logErrorToMyService(error, errorInfo)
        console.log(error)
        console.log(errorInfo)
    }

    render() {
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return <h2>An error occurred.</h2>
        }

        return this.props.children
    }
}

export default App
