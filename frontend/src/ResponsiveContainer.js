/* global gapi */
import React, { Component } from 'react'
import {
    Icon,
    Sidebar,
    Responsive,
    Button,
    Menu,
    Header,
    Segment,
    Dropdown,
    Image
} from 'semantic-ui-react'
import { Link, withRouter } from 'react-router-dom'
import util from './util'
import { GoogleLogin } from 'react-google-login'
import swal from 'sweetalert'
import * as m from './msgDict'
import defaultProfpic from './images/defaultProfpic.png'

class ResponsiveContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            redirectUrl: '',
            isLoggedIn: util.getObject('token') ? true : false,
            sidebarOpened: false
        }
        this.onSignOut = this.onSignOut.bind(this)
    }
    componentDidMount() {
        const query = new URLSearchParams(this.props.location.search)
        const redirectUrl = query.get('redirect') ? query.get('redirect') : ''
        const groupName = query.get('group') ? query.get('group') : ''

        if (redirectUrl) {
            if (util.getObject('token')) {
                this.setState({ isLoggedIn: true }, function() {
                    this.props.history.push(redirectUrl)
                })
            } else if (groupName) {
                swal(
                    m.CustomInfo(`You may sign in first to join ${groupName}.`)
                )
                this.setState({ redirectUrl: redirectUrl })
            }
        }
    }
    // google sign in
    onSignIn = googleUser => {
        const { redirectUrl } = this.state

        const endpoint = util.createEndpoint('/googlesignin')
        const request = new Request(endpoint, {
            method: 'POST',
            body: JSON.stringify({ idtoken: googleUser.tokenId }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        util.putObject('user', result.user)
                        util.putObject('token', result.token)
                        this.setState(
                            { isLoggedIn: true },
                            redirectUrl
                                ? this.props.history.push(redirectUrl)
                                : function() {}
                        )
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .catch(error => swal(m.NetworkError()))
    }

    onSignOut = () => {
        util.deleteObject(['user', 'token'])
        this.setState({ isLoggedIn: false })
    }

    handleSidebar = () => {
        this.setState(prevState => ({
            sidebarOpened: !prevState.sidebarOpened
        }))
    }
    render() {
        const { isLoggedIn, sidebarOpened } = this.state
        return (
            <Segment basic style={{ minHeight: '100vh', marginTop: '50px' }}>
                <Responsive {...Responsive.onlyMobile}>
                    <Sidebar
                        as={Menu}
                        animation="overlay"
                        icon="labeled"
                        onHide={this.handleSidebar}
                        vertical
                        visible={sidebarOpened}
                    >
                        <Menu.Item as={Link} to="/home">
                            Home
                        </Menu.Item>
                        <Menu.Item as={Link} to="/organization">
                            Organizations
                        </Menu.Item>
                        {!isLoggedIn && (
                            <Menu.Item>
                                <GoogleSignInButton onSuccess={this.onSignIn} />
                            </Menu.Item>
                        )}
                    </Sidebar>

                    <Sidebar.Pusher>
                        <Menu
                            text
                            fixed="top"
                            style={{
                                padding: '1%',
                                margin: '0',
                                backgroundColor: 'white'
                            }}
                            size="huge"
                        >
                            <Menu.Item onClick={this.handleSidebar}>
                                <Icon name="sidebar" />
                            </Menu.Item>
                            <Menu.Item position="right">
                                {isLoggedIn && (
                                    <UserDropdown signout={this.onSignOut} />
                                )}
                            </Menu.Item>
                        </Menu>
                        {this.props.children}
                    </Sidebar.Pusher>
                </Responsive>
                <Responsive minWidth={Responsive.onlyTablet.minWidth}>
                    <Menu
                        text
                        fixed="top"
                        style={{
                            padding: '0.5%',
                            margin: '0',
                            backgroundColor: 'white',
                            boxShadow: '0 4px 8px 0 rgba(36,50,66,.075)'
                        }}
                        size="huge"
                    >
                        <Menu.Item>
                            <a href="/">
                                <Header as="h1" color="blue">
                                    Groupible
                                    {/*
                                    <Image src={logo} />
                                    */}
                                </Header>
                            </a>
                        </Menu.Item>
                        <div className="g-signin2" />
                        <Menu.Item as={Link} to="/home">
                            Home
                        </Menu.Item>
                        <Menu.Item as={Link} to="/organization">
                            Organizations
                        </Menu.Item>
                        <Menu.Item position="right">
                            {isLoggedIn ? (
                                <UserDropdown signout={this.onSignOut} />
                            ) : (
                                <GoogleSignInButton onSuccess={this.onSignIn} />
                            )}
                        </Menu.Item>
                    </Menu>
                    {this.props.children}
                </Responsive>
            </Segment>
        )
    }
}

function UserDropdown(props) {
    const user = util.getObject('user')
    const userOptions = [
        {
            key: 'account',
            text: 'My Account',
            slug: '/account'
        },
        {
            key: 'profile',
            text: 'Public Profile',
            slug: `/user/${user.id}`
        },
        {
            key: 'newfeed',
            text: 'New Feed',
            slug: '/new-feed'
        },
        {
            key: 'newgroup',
            text: 'New Group',
            slug: '/new-group'
        },
        {
            key: 'signout',
            text: 'Sign Out',
            onClick: () => props.signout()
        }
    ]
    let options = []
    userOptions.forEach(option => {
        options.push({
            key: option.key,
            to: option.slug,
            text: option.text,
            as: Link,
            onClick: option.onClick,
            active: false,
            selected: false,
            disabled: option.key === 'user' ? true : false
        })
    })
    return (
        <Dropdown
            trigger={
                <span>
                    <Image
                        avatar
                        src={
                            user.profpic_key
                                ? util.createFileUrl(user.profpic_key)
                                : defaultProfpic
                        }
                    />{' '}
                    {user.firstname} {user.lastname}
                </span>
            }
            options={userOptions.map(option => (
                <Menu.Item
                    key={option.key}
                    as={Link}
                    to={option.slug}
                    onClick={option.onClick}
                >
                    {option.text}
                </Menu.Item>
            ))}
            pointing="top right"
        />
    )
}
const GoogleSignInButton = props => (
    <GoogleLogin
        className="googlesignin"
        clientId="560618817415-t6j9tj0t7e8710ndc9elv0kg7usqn0ki.apps.googleusercontent.com"
        buttonText="Continue with Google"
        onSuccess={props.onSuccess}
    />
)

export default withRouter(ResponsiveContainer)
