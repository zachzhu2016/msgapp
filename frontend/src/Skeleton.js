import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
    Button,
    Icon,
    Menu,
    Header,
    Responsive,
    Segment,
    Sidebar,
    Visibility,
    Dropdown
} from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import util from './util'

export default function ResponsiveContainer({ children }) {
    return (
        <div>
            <DesktopContainer>
                <Segment
                    basic
                    style={{ minHeight: '100vh', marginTop: '50px' }}
                >
                    {children}
                </Segment>
            </DesktopContainer>
            <MobileContainer>
                <Segment basic style={{ minHeight: '100vh' }}>
                    {children}
                </Segment>
            </MobileContainer>
        </div>
    )
}

ResponsiveContainer.propTypes = {
    children: PropTypes.node
}

const getWidth = () => {
    const isSSR = typeof window === 'undefined'
    return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth
}

class DesktopContainer extends Component {
    state = {}

    hideFixedMenu = () => this.setState({ fixed: false })
    showFixedMenu = () => this.setState({ fixed: true })

    render() {
        const { children } = this.props
        const { fixed } = this.state
        const user = util.getObject('user')

        return (
            <Responsive
                getWidth={getWidth}
                minWidth={Responsive.onlyTablet.minWidth}
            >
                <Visibility
                    once={false}
                    onBottomPassed={this.showFixedMenu}
                    onBottomPassedReverse={this.hideFixedMenu}
                >
                    <Menu
                        text
                        fixed="top"
                        style={{
                            padding: '1%',
                            margin: '0',
                            backgroundColor: 'white'
                        }}
                        size="large"
                    >
                        <Menu.Item>
                            <a href="/">
                                <Header as="h2">GROUPIBLE</Header>
                            </a>
                        </Menu.Item>
                        <Menu.Item as={Link} to="/home">
                            Home
                        </Menu.Item>
                        <Menu.Item as={Link} to="/organization">
                            Organizations
                        </Menu.Item>
                        <Menu.Item position="right">
                            {renderDesktopMenu(user, fixed)}
                        </Menu.Item>
                    </Menu>
                </Visibility>

                {children}
            </Responsive>
        )
    }
}

DesktopContainer.propTypes = {
    children: PropTypes.node
}

function renderDesktopMenu(user, fixed) {
    if (!user) {
        return <GuestRightMenu />
    } else {
        const userOptions = getUserOptions(user)
        let options = []
        userOptions.forEach(option => {
            options.push({
                key: option.key,
                to: option.slug,
                text: option.text,
                as: Link,
                onClick: option.onClick,
                active: false,
                selected: false,
                disabled: option.key === 'user' ? true : false
            })
        })
        const trigger = (
            <span>
                hello, {user.firstname} {user.lastname}
            </span>
        )
        return (
            <Menu.Item>
                <Dropdown
                    trigger={trigger}
                    options={options}
                    pointing="top right"
                />
            </Menu.Item>
        )
    }
}

class MobileContainer extends Component {
    state = {}

    handleSidebarHide = () => this.setState({ sidebarOpened: false })

    handleToggle = () => this.setState({ sidebarOpened: true })

    render() {
        const { children } = this.props
        const { sidebarOpened } = this.state
        const user = util.getObject('user')

        return (
            <Responsive
                as={Sidebar.Pushable}
                getWidth={getWidth}
                maxWidth={Responsive.onlyMobile.maxWidth}
            >
                <Sidebar
                    as={Menu}
                    animation="push"
                    onHide={this.handleSidebarHide}
                    vertical
                    visible={sidebarOpened}
                >
                    {renderMobileMenu(user)}
                </Sidebar>
                <Sidebar.Pusher dimmed={sidebarOpened}>
                    <Menu
                        fixed="top"
                        text
                        size="large"
                        style={{ padding: '1%', margin: '0' }}
                    >
                        <Menu.Item onClick={this.handleToggle}>
                            <Icon name="sidebar" />
                        </Menu.Item>
                        {/*
                        <Menu.Item position="right">
                            <GuestRightMenu />
                        </Menu.Item>
                        */}
                    </Menu>
                    {children}
                </Sidebar.Pusher>
            </Responsive>
        )
    }
}

MobileContainer.propTypes = {
    children: PropTypes.node
}

function renderMobileMenu(user) {
    const options = user ? getUserOptions(user) : getGuestOptions()
    return (
        <>
            <Menu.Item as={Link} to="/home">
                Home
            </Menu.Item>
            <Menu.Item as={Link} to="/organization">
                Organizations
            </Menu.Item>
            {options.map(option => (
                <Menu.Item
                    key={option.key}
                    as={Link}
                    to={option.slug}
                    onClick={option.onClick}
                >
                    {option.text}
                </Menu.Item>
            ))}
        </>
    )
}

const GuestRightMenu = () => {
    return (
        <>
            <Menu.Item as={Link} to="/login">
                Sign in
            </Menu.Item>
            <Menu.Item as={Link} to="/register">
                Sign up
            </Menu.Item>
            {/*
            <Button basic compact as={Link} to="/login">
                Sign In
            </Button>
            <Button
                as={Link}
                size="tiny"
                to="/register"
                basic
                compact
                style={{ marginLeft: '0.5em' }}
            >
                Sign Up
            </Button>*/}
        </>
    )
}

function getGuestOptions() {
    const options = [
        {
            key: 'signin',
            text: 'Sign In',
            slug: '/login'
        },
        {
            key: 'signup',
            text: 'Sign Up',
            slug: '/register'
        }
    ]
    return options
}

function getUserOptions(user) {
    const options = [
        {
            key: 'profile',
            text: 'Your Profile',
            slug: `/user/${user.id}`
        },
        {
            key: 'account',
            text: 'Your Account',
            slug: '/account'
        },
        {
            key: 'newfeed',
            text: 'New Feed',
            slug: '/new-feed'
        },
        {
            key: 'newgroup',
            text: 'New Group',
            slug: '/new-group'
        },
        {
            key: 'signout',
            text: 'Sign Out',
            onClick: function() {
                util.signout(window.location.reload())
            }
        }
    ]
    return options
}
