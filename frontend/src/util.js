import React, { Component } from 'react'

const util = {
    displayFileSize(sizeInByte) {
        let result = 'Invalid Size'
        if (sizeInByte > 0 && sizeInByte < 1000) {
            result = `Size: ${sizeInByte} B`
        } else if (sizeInByte > 1000 && sizeInByte < 1000 * 1000) {
            const sizeInKB = sizeInByte / 1000
            result = `Size: ${sizeInKB} KB`
        } else if (
            sizeInByte > 1000 * 1000 &&
            sizeInByte < 1000 * 1000 * 1000
        ) {
            const sizeInMB = sizeInByte / (1000 * 1000)
            result = `Size: ${sizeInMB} MB`
        }
        return result
    },
    displayTimeDiff(timestamp) {
        const before = new Date(timestamp)
        const now = new Date().getTime()

        //TODO make it more flexible in the future
        let isSafari = /^((?!chrome|android).)*safari/i.test(
            navigator.userAgent
        )
        //alert(isSafari)
        //const offset = isSafari ? 0 : new Date().getTimezoneOffset() * 60 * 1000
        const offset = 0
        const diff = now - before + offset

        const oneDay = 24 * 60 * 60 * 1000
        const oneHour = 60 * 60 * 1000
        const oneMinute = 60 * 1000

        const diffDays = Math.round(Math.abs(diff / oneDay))
        const diffHours = Math.round(Math.abs(diff / oneHour))
        const diffMinutes = Math.round(Math.abs(diff / oneMinute))

        if (diffDays > 0) {
            return `(${diffDays} ${diffDays === 1 ? 'day' : 'days'} ago)`
        } else if (diffHours > 0) {
            return `(${diffHours} ${diffHours === 1 ? 'hour' : 'hours'} ago)`
        } else if (diffMinutes > 0) {
            return `(${diffMinutes} ${
                diffMinutes === 1 ? 'minute' : 'minutes'
            } ago)`
        } else {
            return '(1 minute ago)'
        }
    },
    putObject(name, object) {
        if (typeof object === 'object') {
            window.localStorage.setItem(name, JSON.stringify(object))
        } else {
            window.localStorage.setItem(name, object)
        }
    },
    getObject(name) {
        let object = window.localStorage.getItem(name)
        try {
            object = JSON.parse(object)
        } catch (e) {}
        return object
    },
    deleteObject(names) {
        for (const name of names) {
            window.localStorage.removeItem(name)
        }
    },
    // this function is for debugging only
    createEndpoint(path) {
        //const domain = 'https://api.groupible.com' // production API
        const domain = 'http://localhost:80' // local testing API
        return domain + path
    },
    createFileUrl(key) {
        const domain = 'https://groupible.s3.amazonaws.com/'
        return domain + key
    }
}
export default util
