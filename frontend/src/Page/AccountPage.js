import React, { Component } from 'react'
import {
    Header,
    Form,
    TextArea,
    Accordion,
    Image,
    Segment,
    Button,
    Menu
} from 'semantic-ui-react'
import ItemList from '../Component/ItemList'
import PageLoader from '../Component/PageLoader'
import util from '../util'
import swal from 'sweetalert'
import * as m from '../msgDict'
import defaultProfpic from '../images/defaultProfpic.png'

export default class AccountPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeTab: 'My Feeds'
        }
    }

    handleItemClick = (e, { name }) => {
        this.setState({ activeTab: name })
    }

    render() {
        let tabContent = null
        const myFeedEndpoint = util.createEndpoint('/feed?view=mine')
        const myGroupEndpoint = util.createEndpoint('/group?view=mine')
        const { activeTab } = this.state
        switch (activeTab) {
            case 'My Feeds':
                tabContent = (
                    <ItemList itemName="myFeed" endpoint={myFeedEndpoint} />
                )
                break
            case 'My Groups':
                tabContent = (
                    <ItemList itemName="group" endpoint={myGroupEndpoint} />
                )
                break
            default:
                alert('Invalid Tab')
        }
        return (
            <Segment basic>
                <UserInfoForm />
                <Menu pointing secondary>
                    <Menu.Item
                        name="My Feeds"
                        active={activeTab === 'My Feeds'}
                        onClick={this.handleItemClick}
                    />
                    <Menu.Item
                        name="My Groups"
                        active={activeTab === 'My Groups'}
                        onClick={this.handleItemClick}
                    />
                </Menu>
                {tabContent}
            </Segment>
        )
    }
}

class UserInfoForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            firstname: '',
            lastname: '',
            email: '',
            about: '',
            profpicKey: '',
            porfpic: '',
            isLoading: false
        }
    }

    componentDidMount() {
        const user = util.getObject('user')
        if (!user) {
            alert('User does not exist.')
            util.signout()
        } else {
            this.setState({
                firstname: user.firstname,
                lastname: user.lastname,
                about: user.about,
                email: user.email,
                profpicKey: user.profpic_key
            })
        }
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    fileInputRef = React.createRef()
    fileChange = e => {
        const newFile = e.target.files[0]
        var reader = new FileReader()
        reader.onload = function() {
            document.getElementById('profpic').src = reader.result
        }
        this.setState({ profpic: newFile })
        reader.readAsDataURL(newFile)
    }

    handleSubmit = () => {
        const { about, profpic } = this.state
        const endpoint = util.createEndpoint('/user/update')

        const formData = new FormData()
        formData.append('about', about)
        formData.append('file', profpic)

        const request = new Request(endpoint, {
            method: 'PUT',
            body: formData,
            enctype: 'multipart/form-data',
            headers: {
                Authorization: util.getObject('token')
            }
        })
        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        util.putObject('user', result.user)
                        swal(m.UserUpdateSuccess())
                        this.setState({
                            about: result.user.about,
                            profpicKey: result.profpic_key
                        })
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .then(() => this.setState({ isLoading: false }))
            .catch(error => swal(m.NetworkError()))
    }
    render() {
        const {
            firstname,
            lastname,
            email,
            about,
            profpicKey,
            isLoading
        } = this.state
        return (
            <Segment textAlign="center" basic>
                {isLoading && (
                    <PageLoader> Updating your informations...</PageLoader>
                )}
                <Form>
                    <Form.Field>
                        <Image
                            avatar
                            style={{ 'font-size': 50 }}
                            id="profpic"
                            src={
                                profpicKey
                                    ? util.createFileUrl(profpicKey)
                                    : defaultProfpic
                            }
                            centered
                        />
                    </Form.Field>
                    <Form.Field>
                        <Button
                            basic
                            content="change"
                            compact
                            onClick={() => this.fileInputRef.current.click()}
                        />
                    </Form.Field>
                    <Header
                        as="h2"
                        content={`${firstname} ${lastname}`}
                        subheader={email}
                    />
                    <Form.Field>
                        <TextArea
                            placeholder="Tell people more about yourself..."
                            onChange={this.handleChange}
                            value={about ? about : ''}
                            name="about"
                            rows="4"
                            style={{
                                'white-space': 'pre-line',
                                padding: 'none',
                                maxWidth: '600px'
                            }}
                        />
                    </Form.Field>
                    <Button
                        primary
                        onClick={() =>
                            this.setState(
                                { isLoading: true },
                                this.handleSubmit
                            )
                        }
                    >
                        Submit All Changes
                    </Button>
                </Form>
                <input
                    ref={this.fileInputRef}
                    type="file"
                    name="attachment"
                    multiple
                    hidden
                    onChange={this.fileChange}
                />
            </Segment>
        )
    }
}
