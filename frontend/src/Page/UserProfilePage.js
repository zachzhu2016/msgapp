import React, { Component } from 'react'
import {
    Grid,
    Image,
    Menu,
    Header,
    Segment,
    Icon,
    Message
} from 'semantic-ui-react'
import { withRouter } from 'react-router-dom'
import util from '../util'
import ItemList from '../Component/ItemList'
import swal from 'sweetalert'
import * as m from '../msgDict'
import defaultProfpic from '../images/defaultProfpic.png'
import PageLoader from '../Component/PageLoader'

class UserProfilePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: null,
            isLoading: true,
            activeTab: 'User Feeds'
        }
    }

    componentDidMount() {
        const userId = this.props.match.params.userId
        const endpoint = util.createEndpoint(`/user/${userId}`)
        const request = new Request(endpoint, {
            method: 'GET',
            headers: {
                Authorization: util.getObject('token')
            }
        })
        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        this.setState({
                            isLoading: false,
                            user: result.user
                        })
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .catch(error => swal(m.NetworkError()))
    }

    handleItemClick = (e, { name }) => {
        this.setState({ activeTab: name })
    }

    render() {
        const { user, isLoading, activeTab } = this.state

        if (isLoading) {
            return <PageLoader>Fetching that content for you.</PageLoader>
        }
        let itemName = null
        let endpoint = null
        switch (activeTab) {
            case 'User Feeds':
                itemName = 'feed'
                endpoint = util.createEndpoint(`/user/${user.id}/feed`)
                break
            case 'User Groups':
                itemName = 'group'
                endpoint = util.createEndpoint(`/user/${user.id}/group`)
                break
        }
        return (
            <Segment basic>
                <Grid textAlign="center">
                    <Grid.Row>
                        <Image
                            avatar
                            style={{ 'font-size': 50 }}
                            src={
                                user.profpic_key
                                    ? util.createFileUrl(user.profpic_key)
                                    : defaultProfpic
                            }
                        />
                    </Grid.Row>
                    <Grid.Row>
                        <Header as="h2" icon>
                            {`${user.firstname} ${user.lastname}`}
                            <Header.Subheader
                                style={{
                                    'white-space': 'pre-line'
                                }}
                                dangerouslySetInnerHTML={{
                                    __html: user.about
                                }}
                            ></Header.Subheader>
                        </Header>
                    </Grid.Row>
                </Grid>
                <Menu pointing secondary>
                    <Menu.Item
                        name="User Feeds"
                        active={activeTab === 'User Feeds'}
                        onClick={this.handleItemClick}
                    />
                    <Menu.Item
                        name="User Groups"
                        active={activeTab === 'User Groups'}
                        onClick={this.handleItemClick}
                    />
                </Menu>
                <ItemList itemName={itemName} endpoint={endpoint} />
            </Segment>
        )
    }
}
export default withRouter(UserProfilePage)
