import React, { Component } from 'react'
import {
    Form,
    Grid,
    Header,
    Message,
    Segment,
    Dimmer,
    Loader
} from 'semantic-ui-react'
import { useHistory } from 'react-router-dom'
import util from '../util'
import swal from 'sweetalert'
import * as m from '../msgDict'

export default function RegisterPage() {
    const history = useHistory()
    return <RegisterPageComponent history={history} />
}

class RegisterPageComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            firstname: '',
            lastname: '',
            email: '',
            password: '',
            isLoading: false
        }
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    submitRegister = () => {
        const { firstname, lastname, email, password } = this.state
        if (!(firstname && lastname && email && password)) {
            return swal(m.ArgumentError()).then(() =>
                this.setState({
                    isLoading: false
                })
            )
        }
        const endpoint = util.createEndpoint('/signup')

        const request = new Request(endpoint, {
            method: 'POST',
            body: JSON.stringify({
                firstname: firstname,
                lastname: lastname,
                email: email,
                password: password
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        swal(m.SignUpSuccess())
                        this.props.history.replace('/login')
                        break
                    default:
                        swal(m.ServerError(result))
                        this.setState({
                            isLoading: false
                        })
                }
            })
            .catch(error => swal(m.NetworkError()))
    }

    render() {
        const { firstname, lastname, email, password, isLoading } = this.state
        return (
            <Segment basic>
                <Grid
                    textAlign="center"
                    style={{ height: '80vh' }}
                    verticalAlign="middle"
                >
                    <Grid.Column style={{ maxWidth: 450 }}>
                        {isLoading && (
                            <Dimmer active inverted>
                                <Loader inverted></Loader>
                            </Dimmer>
                        )}
                        <Segment padded="very" piled>
                            <Form size="large" onSubmit={this.submitRegister}>
                                <Form.Input
                                    fluid
                                    onChange={this.handleChange}
                                    placeholder="Firstname"
                                    name="firstname"
                                    value={firstname}
                                />
                                <Form.Input
                                    fluid
                                    onChange={this.handleChange}
                                    placeholder="Lastname"
                                    name="lastname"
                                    value={lastname}
                                />
                                <Form.Input
                                    fluid
                                    onChange={this.handleChange}
                                    placeholder="Email address"
                                    name="email"
                                    type="email"
                                    value={email}
                                />
                                <Form.Input
                                    onChange={this.handleChange}
                                    fluid
                                    placeholder="Password"
                                    type="password"
                                    name="password"
                                    value={password}
                                />
                                <Form.Button
                                    primary
                                    fluid
                                    size="large"
                                    content="Sign Up"
                                    icon="right arrow"
                                    labelPosition="right"
                                    onClick={() =>
                                        this.setState(
                                            { isLoading: true },
                                            this.handleSubmit
                                        )
                                    }
                                />
                            </Form>
                        </Segment>
                    </Grid.Column>
                </Grid>
            </Segment>
        )
    }
}
