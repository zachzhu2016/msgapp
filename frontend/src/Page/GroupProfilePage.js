import React, { Component } from 'react'
import { Menu, Icon, Segment, Header, Button } from 'semantic-ui-react'
import { withRouter, useLocation } from 'react-router-dom'
import util from '../util'
import ItemList from '../Component/ItemList'
import InviteModal from '../Component/InviteModal'
import swal from 'sweetalert'
import * as m from '../msgDict'
import PageLoader from '../Component/PageLoader'

class GroupProfilePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            group: null,
            isLoading: true,
            activeTab: 'Feeds',
            actionGroupState: null
        }
    }

    fetchPageInfoWithInvite(code, groupId) {
        const endpoint = util.createEndpoint(
            `/group/${groupId}/invitation/accept?invite_code=${code}`
        )
        const request = new Request(endpoint, {
            method: 'GET',
            headers: {
                Authorization: util.getObject('token')
            }
        })

        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        swal(m.JoinGroupSuccess())
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .then(() => this.fetchPageInfo())
            .catch(error => swal(m.NetworkError()))
    }

    componentDidMount() {
        const query = new URLSearchParams(this.props.location.search)
        const inviteCode = query.get('invite_code')
            ? query.get('invite_code')
            : ''

        if (inviteCode) {
            const history = this.props.history
            const groupId = this.props.match.params.groupId
            this.fetchPageInfoWithInvite(inviteCode, groupId)
            //ensures that inviteCode only gets executed once
            history.replace(`/group/${groupId}`)
        } else {
            this.fetchPageInfo()
        }
    }

    fetchPageInfo() {
        const groupId = this.props.match.params.groupId
        const groupEndpoint = util.createEndpoint(`/group/${groupId}`)
        const funcEndpoint = util.createEndpoint(
            `/finder?func=relation&groupId=${groupId}`
        )

        const groupRequest = new Request(groupEndpoint, {
            method: 'GET',
            headers: {
                Authorization: util.getObject('token')
            }
        })
        const funcRequest = new Request(funcEndpoint, {
            method: 'GET',
            headers: {
                Authorization: util.getObject('token')
            }
        })

        Promise.all([fetch(groupRequest), fetch(funcRequest)])
            .then(([groupRes, funcRes]) =>
                Promise.all([groupRes.json(), funcRes.json()])
            )
            .then(([groupResult, funcResult]) => {
                if (groupResult.status === 1 && funcResult.status === 1) {
                    this.setState({
                        group: groupResult.group,
                        isLoading: false,
                        actionState: funcResult.relation
                    })
                } else if (groupResult.status !== 1) {
                    swal(m.ServerError(groupResult))
                } else if (funcResult.status !== 1) {
                    swal(m.ServerError(funcResult))
                }
            })
            .catch(error => {
                swal(m.NetworkError())
            })
    }

    handleItemClick = (e, { name }) => {
        this.setState({ activeTab: name })
    }

    render() {
        const { group, actionState, isLoading, activeTab } = this.state
        const groupId = this.props.match.params.groupId

        if (isLoading) {
            return <PageLoader>Fetching that content for you.</PageLoader>
        }

        let ActionButtonGroup = null
        if (actionState === 'out' || actionState === 'none') {
            ActionButtonGroup = (
                <Button
                    content="Join Group"
                    onClick={() => groupAction(groupId, group.name, 'join')}
                    compact
                    primary
                />
            )
        } else if (actionState === 'in') {
            ActionButtonGroup = (
                <>
                    <Button
                        content="Leave Group"
                        onClick={() => {
                            if (
                                window.confirm('Do you really want to leave?')
                            ) {
                                groupAction(groupId, group.name, 'leave')
                            }
                        }}
                        compact
                        tiny
                    />
                    <InviteModal showModal={false} groupId={groupId} />
                </>
            )
        }

        let itemName = null
        let endpoint = null
        switch (activeTab) {
            case 'Feeds':
                itemName = 'feed'
                endpoint = util.createEndpoint(`/group/${groupId}/feed`)
                break
            case 'Users':
                itemName = 'user'
                endpoint = util.createEndpoint(`/group/${groupId}/user`)
                break
            case 'Files':
                itemName = 'file'
                endpoint = util.createEndpoint(`/group/${groupId}/file`)
                break
            default:
        }
        return (
            <Segment basic>
                <Header as="h2" icon textAlign="center">
                    <Icon name="users" circular />
                    {group.name}
                    <Header.Subheader>
                        {`A ${
                            group.is_public === 1 ? 'public' : 'private'
                        } group created by`}{' '}
                        <a
                            href="#"
                            onClick={() => {
                                window.event.preventDefault()
                                this.props.history.push(
                                    `/user/${group.creator.id}`
                                )
                            }}
                        >
                            {`${group.creator.firstname} ${group.creator.lastname}
                            `}
                        </a>
                    </Header.Subheader>
                    <Header.Subheader>{group.about}</Header.Subheader>
                    {ActionButtonGroup}
                </Header>
                <div>
                    <Menu pointing secondary>
                        <Menu.Item
                            name="Feeds"
                            active={activeTab === 'Feeds'}
                            onClick={this.handleItemClick}
                        />
                        <Menu.Item
                            name="Users"
                            active={activeTab === 'Users'}
                            onClick={this.handleItemClick}
                        />
                        <Menu.Item
                            name="Files"
                            active={activeTab === 'Files'}
                            onClick={this.handleItemClick}
                        />
                    </Menu>
                </div>
                <ItemList itemName={itemName} endpoint={endpoint} />
            </Segment>
        )
    }
}

/*
class DocumentTab extends Component {
    constructor(props) {
        super(props)
        this.state = {
            fileList: [],
            documents: []
        }
    }
    componentDidMount() {
        const groupId = this.props.match.params.groupId
        const endpoint = util.createEndpoint(`/group/${groupId}/document`)
        const request = new Request(endpoint, {
            method: 'GET',
            headers: {
                Authorization: util.getObject('token')
            }
        })
        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        this.setState({ documents: result.documents })
                    default:
                        swal(m.ServerError(result))
                }
            })
            .catch(error => swal(m.NetworkError()))
    }
    fileInputRef = React.createRef()
    render() {
        return (
            <>
                <Button
                    content="Add New Document"
                    icon="upload"
                    labelPosition="left"
                    onClick={() => this.fileInputRef.current.click()}
                />
                <input
                    ref={this.fileInputRef}
                    type="file"
                    name="attachment"
                    multiple
                    hidden
                    onChange={this.fileChange}
                />
            </>
        )
    }
}
*/

function groupAction(groupId, groupName, action) {
    if (action !== 'join' && action !== 'leave') {
        return swal(m.CustomError('Cannot execute this action.'))
    }
    const endpoint = util.createEndpoint(`/group/${groupId}/${action}`)
    const request = new Request(endpoint, {
        method: 'POST',
        headers: {
            Authorization: util.getObject('token')
        }
    })
    fetch(request)
        .then(res => res.json())
        .then(result => {
            switch (result.status) {
                case 1:
                    if (result.action === 'join') {
                        swal(m.JoinGroupSuccess()).then(() =>
                            window.location.reload()
                        )
                    } else if (result.action === 'leave') {
                        swal(m.LeaveGroupSuccess()).then(() =>
                            window.location.reload()
                        )
                    }
                    break
                default:
                    swal(m.ServerError(result))
            }
        })
        .catch(error => {
            swal(m.NetworkError())
        })
}
export default withRouter(GroupProfilePage)
