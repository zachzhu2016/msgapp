import React, { Component } from 'react'
import {
    Container,
    Form,
    TextArea,
    Checkbox,
    Segment,
    Button
} from 'semantic-ui-react'
import { useHistory } from 'react-router-dom'
import util from '../util'
import swal from 'sweetalert'
import * as m from '../msgDict'

export default function NewGroupPage() {
    const history = useHistory()
    return <NewGroupPageComponent history={history} />
}

class NewGroupPageComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            about: '',
            isPublic: false
        }
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    toggle = () =>
        this.setState(prevState => ({ isPublic: !prevState.isPublic }))

    handleSubmit = () => {
        const { name, about, isPublic } = this.state
        if (!name || !about) {
            return swal(m.ArgumentError())
        }

        const endpoint = util.createEndpoint(`/group/new`)
        const request = new Request(endpoint, {
            method: 'POST',
            body: JSON.stringify({
                name: name,
                about: about,
                is_public: isPublic & 1
            }),
            headers: {
                'Content-Type': 'application/json',
                Authorization: util.getObject('token')
            }
        })
        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        swal(m.CreateGroupSuccess(name))
                        const user = util.getObject('user')
                        this.props.history.replace(
                            `/organization/${user.org_id}/explore`
                        )
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .catch(error => swal(m.NetworkError()))
    }

    render() {
        const { name, about } = this.state
        return (
            <Segment basic>
                <Container text>
                    <Form>
                        <Form.Input
                            label="Name"
                            name="name"
                            placeholder="Give your group a name..."
                            value={name}
                            onChange={this.handleChange}
                        />
                        <Form.Input
                            control={TextArea}
                            label="About"
                            name="about"
                            placeholder="Say something about this group..."
                            value={about}
                            onChange={this.handleChange}
                        />
                        <Form.Field>
                            <Checkbox
                                label="Make it a public group (allow read access to the public)."
                                onChange={this.toggle}
                                checked={this.state.isPublic}
                            />
                        </Form.Field>
                        <Form.Field
                            control={Button}
                            primary
                            onClick={this.handleSubmit}
                        >
                            Create New Group
                        </Form.Field>
                    </Form>
                </Container>
            </Segment>
        )
    }
}
