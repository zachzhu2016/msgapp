import React, { Component } from 'react'
import {
    Container,
    Button,
    Divider,
    List,
    Message,
    Segment,
    Dropdown,
    Header,
    Form,
    Input,
    Icon,
    TextArea
} from 'semantic-ui-react'
import util from '../util'
import { useHistory } from 'react-router-dom'
import FlexList from '../Component/FlexList.js'
import TextareaAutosize from 'react-textarea-autosize'
import swal from 'sweetalert'
import * as m from '../msgDict'

export default function NewFeedPage() {
    let history = useHistory()
    return <NewFeedPageComponent history={history} />
}

class NewFeedPageComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title: '',
            body: '',
            group_id: null,
            dropdownList: [],
            fileList: [],
            checkedTag: 'discuss'
        }
        this.handleRemove = this.handleRemove.bind(this)
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })
    handleTextareaChange = e =>
        this.setState({ [e.target.name]: e.target.value })

    handleGroupChange = (e, data) => {
        const { value } = data
        const { key } = data.options.find(o => o.value === value)
        this.setState({ group_id: key })
    }

    handleRemove = filename => {
        this.setState(prevState => ({
            fileList: prevState.fileList.filter(file => file.name !== filename)
        }))
    }

    fileInputRef = React.createRef()

    fileChange = e => {
        this.setState({ fileList: [...this.state.fileList, e.target.files[0]] })
    }

    // to be modulized
    componentDidMount() {
        const endpoint = util.createEndpoint('/group?view=mine')

        const request = new Request(endpoint, {
            method: 'GET',
            headers: {
                Authorization: util.getObject('token')
            }
        })

        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        // restrict
                        if (!result.groups.length) {
                            swal(
                                m.CustomInfo(
                                    'You must be in at least one group to make a post. Join one or create a new group now.'
                                )
                            ).then(() =>
                                this.props.history.replace('/new-group')
                            )
                        }

                        let dropdownList = []
                        for (const group of result.groups) {
                            dropdownList.push({
                                key: group.id,
                                text: group.name,
                                value: group.name
                            })
                        }
                        this.setState({ dropdownList: dropdownList })
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .catch(error => swal(m.NetworkError()))
    }

    handleSubmit = () => {
        const { title, body, checkedTag, group_id, fileList } = this.state

        if (!title || !body || !group_id) {
            return swal(m.ArgumentError())
        }

        const endpoint = util.createEndpoint(`/group/${group_id}/feed/new`)

        const formData = new FormData()
        for (const file of fileList) {
            formData.append('file_list', file)
        }
        formData.append('title', title)
        formData.append('body', body)
        formData.append('tag', checkedTag)
        formData.append('group_id', group_id)

        const request = new Request(endpoint, {
            method: 'POST',
            body: formData,
            enctype: 'multipart/form-data',
            headers: {
                Authorization: util.getObject('token')
            }
        })
        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        swal(m.PostSuccess('message'))
                        this.props.history.replace('/account')
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .catch(error => swal(m.NetworkError()))
    }

    render() {
        const { title, body, fileList, dropdownList, checkedTag } = this.state
        return (
            <Segment padded basic>
                <Container text>
                    <Form>
                        <Form.Group widths="equal">
                            <Form.Input
                                control={Input}
                                name="title"
                                placeholder="Title"
                                onChange={this.handleChange}
                                value={title}
                            />
                        </Form.Group>
                        <Form.Field>
                            <TextareaAutosize
                                name="body"
                                placeholder="Write down what you would like to share : )"
                                value={body}
                                onChange={this.handleTextareaChange}
                                style={{ maxHeight: 300 }}
                            />
                        </Form.Field>
                        <Divider horizontal>
                            <Header as="h4">Attachments</Header>
                        </Divider>
                        {fileList.length ? (
                            <FlexList
                                itemList={fileList}
                                identifier="name"
                                extraProp="size"
                                handleRemove={this.handleRemove}
                            />
                        ) : (
                            <Message
                                icon="inbox"
                                header=""
                                content="No documents are attached in this feed."
                            />
                        )}
                        <Button
                            content="Add New Document"
                            icon="upload"
                            labelPosition="left"
                            onClick={() => this.fileInputRef.current.click()}
                        />
                        <Form.Field>
                            Feel free to upload anything below 20MB.
                        </Form.Field>
                        <Divider />
                        <Form.Field
                            control={Dropdown}
                            placeholder="pick a group to post this to:"
                            selection
                            name="group_id"
                            onChange={this.handleGroupChange}
                            search
                            options={dropdownList}
                        ></Form.Field>
                        <Form.Group inline>
                            <label>Choose a tag:</label>
                            <Form.Radio
                                name="checkedTag"
                                label="Discuss"
                                value="discuss"
                                checked={checkedTag === 'discuss'}
                                onChange={this.handleChange}
                            />
                            <Form.Radio
                                name="checkedTag"
                                label="Question"
                                value="question"
                                checked={checkedTag === 'question'}
                                onChange={this.handleChange}
                            />
                            <Form.Radio
                                name="checkedTag"
                                label="Sharing"
                                value="share"
                                checked={checkedTag === 'share'}
                                onChange={this.handleChange}
                            />
                            <Form.Radio
                                name="checkedTag"
                                label="Announcement / Notice"
                                value="notice"
                                checked={checkedTag === 'notice'}
                                onChange={this.handleChange}
                            />
                        </Form.Group>
                        <input
                            ref={this.fileInputRef}
                            type="file"
                            name="attachment"
                            multiple
                            hidden
                            onChange={this.fileChange}
                        />
                        <Form.Field
                            control={Button}
                            primary
                            onClick={this.handleSubmit}
                        >
                            Post it now
                        </Form.Field>
                    </Form>
                </Container>
            </Segment>
        )
    }
}

/*
class FileList extends Component {
    handleRemove(filename) {
        this.props.handleRemove(filename);
    }
    render() {
        return (
            <List divided verticalAlign="middle">
                {this.props.fileList
                    ? this.props.fileList.map((file, index) => (
                          <List.Item key={index}>
                              <List.Content floated="right">
                                  <Button
                                      compact
                                      onClick={this.handleRemove.bind(
                                          this,
                                          file.name,
                                      )}>
                                      remove
                                  </Button>
                              </List.Content>
                              <List.Content>
                                  <List.Header as="a">{file.name}</List.Header>
                                  <List.Description>
                                      {displayFileSize(file.size)}
                                  </List.Description>
                              </List.Content>
                          </List.Item>
                      ))
                    : ''}
            </List>
        );
    }
}
*/
