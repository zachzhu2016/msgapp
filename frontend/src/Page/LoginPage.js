import React, { Component } from 'react'
import {
    Segment,
    Form,
    Grid,
    Message,
    Divider,
    Header
} from 'semantic-ui-react'
import { withRouter, Redirect, useHistory, useLocation } from 'react-router-dom'
import util from '../util'
//import { GoogleSignInButton } from '../Component/GoogleSignInButton'
import { GoogleLogin } from 'react-google-login'
import swal from 'sweetalert'
import * as m from '../msgDict'
import PageLoader from '../Component/PageLoader'

function useQuery() {
    return new URLSearchParams(useLocation().search)
}

class LoginPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            redirectUrl: null,
            isLoading: false
        }
    }

    componentDidMount() {
        const query = new URLSearchParams(this.props.location.search)
        const redirectUrl = query.get('redirect') ? query.get('redirect') : ''

        if (redirectUrl) {
            if (util.getObject('token')) {
                this.props.history.push(redirectUrl)
            } else {
                this.props.history.replace('/login') // remove redirect from url
                this.setState({ redirectUrl: redirectUrl })
            }
        }
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    handleSubmit = () => {
        const { email, password, redirectUrl } = this.state

        if (!(email && password)) {
            return swal(m.ArgumentError()).then(() =>
                this.setState({
                    isLoading: false
                })
            )
        }

        const endpoint = util.createEndpoint('/signin')
        const request = new Request(endpoint, {
            method: 'POST',
            body: JSON.stringify({
                email: email,
                password: password
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })

        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        window.localStorage.setItem(
                            'user',
                            JSON.stringify(result.user)
                        )
                        window.localStorage.setItem('token', result.token)
                        this.props.history.push(
                            redirectUrl ? redirectUrl : '/account'
                        )
                        window.location.reload()
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .catch(error => swal(m.NetworkError()))
            .then(() =>
                this.setState({
                    isLoading: false
                })
            )
    }

    // google sign in
    onSignIn = googleUser => {
        const { redirectUrl } = this.props

        const endpoint = util.createEndpoint('/googlesignin')
        const request = new Request(endpoint, {
            method: 'POST',
            body: JSON.stringify({ idtoken: googleUser.tokenId }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        window.localStorage.setItem(
                            'user',
                            JSON.stringify(result.user)
                        )
                        window.localStorage.setItem('token', result.token)
                        this.props.history.push(
                            redirectUrl ? redirectUrl : '/account'
                        )
                        window.location.reload()
                    default:
                        swal(m.ServerError(result))
                        this.setState({
                            isLoading: false
                        })
                }
            })
            .catch(error => swal(m.NetworkError()))
    }

    render() {
        let { email, password, isLoading } = this.state

        return (
            <Segment basic>
                <Grid
                    textAlign="center"
                    style={{ height: '80vh' }}
                    verticalAlign="middle"
                >
                    <Grid.Column style={{ maxWidth: 450 }}>
                        {isLoading && <PageLoader />}
                        <Segment padded="very" piled>
                            <Grid centered>
                                <Form size="large" style={{ width: '300px' }}>
                                    <Form.Field>
                                        <GoogleSignInButton
                                            onSuccess={this.onSignIn}
                                            onFailure={this.onFailure}
                                        />
                                    </Form.Field>
                                    <Divider horizontal>Or</Divider>
                                    <Form.Input
                                        fluid
                                        onChange={this.handleChange}
                                        placeholder="Email address"
                                        name="email"
                                        type="email"
                                        value={email}
                                    />
                                    <Form.Input
                                        fluid
                                        onChange={this.handleChange}
                                        placeholder="Password"
                                        type="password"
                                        name="password"
                                        value={password}
                                    />
                                    <Form.Button
                                        primary
                                        fluid
                                        size="large"
                                        content="Sign In"
                                        onClick={() =>
                                            this.setState(
                                                { isLoading: true },
                                                this.handleSubmit
                                            )
                                        }
                                    />
                                </Form>
                            </Grid>
                        </Segment>
                    </Grid.Column>
                </Grid>
            </Segment>
        )
    }
}

const GoogleSignInButton = props => {
    return (
        <GoogleLogin
            className="googlesignin"
            clientId="560618817415-t6j9tj0t7e8710ndc9elv0kg7usqn0ki.apps.googleusercontent.com"
            buttonText="Continue with Google"
            onSuccess={props.onSuccess}
            onFailure={props.onFailure}
        />
    )
}
export default withRouter(LoginPage)
