import React, { Component } from 'react'
import {
    List,
    Card,
    Image,
    Label,
    Button,
    Container,
    Comment,
    Icon,
    Form,
    Segment,
    Header,
    Message
} from 'semantic-ui-react'
import { withRouter, Link, useParams, useHistory } from 'react-router-dom'
import util from '../util'
import swal from 'sweetalert'
import * as m from '../msgDict'
import ItemList from '../Component/ItemList'

class FeedPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            item: null,
            commentList: [],
            isLoading: true
        }
    }

    componentDidMount() {
        const itemId = this.props.match.params.itemId
        const endpoint = util.createEndpoint(`/feed/${itemId}`)

        const request = new Request(endpoint, {
            method: 'GET',
            headers: {
                Authorization: util.getObject('token')
            }
        })

        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        this.setState({
                            item: result.feed,
                            isLoading: false
                        })
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .catch(error => swal(m.NetworkError()))
    }

    render() {
        const { item, commentList, isLoading } = this.state

        if (isLoading) {
            return (
                <Message icon>
                    <Icon name="circle notched" loading />
                    <Message.Content>
                        <Message.Header>Just one second</Message.Header>
                        We are fetching that content for you.
                    </Message.Content>
                </Message>
            )
        }

        const itemId = this.props.match.params.itemId
        const commentEndpoint = util.createEndpoint(`/feed/${itemId}/comment`)
        return (
            <Container text>
                <Article item={item} history={this.props.history} />
                <Comment.Group style={{ 'max-width': 'none' }}>
                    <CommentForm item={item} />
                    <Header as="h3" dividing>
                        Comments
                    </Header>
                    <ItemList itemName="comment" endpoint={commentEndpoint} />
                </Comment.Group>
            </Container>
        )
    }
}

const Article = props => {
    const history = props.history
    const item = props.item
    if (!item) {
        return <> </>
    }
    return (
        <>
            <List>
                <List.Item>
                    <Header as="h1">
                        <Header.Content>{item.title}</Header.Content>
                    </Header>
                    <List.Description>
                        {'Posted in '}
                        <a
                            onClick={() =>
                                history.push(`/group/${item.group_id}`)
                            }
                        >{`${item.group.name}`}</a>
                        {' by '}
                        <a
                            onClick={() =>
                                history.push(`/user/${item.user_id}`)
                            }
                        >{`${item.author.firstname} ${item.author.lastname}`}</a>
                        {` ${util.displayTimeDiff(item.created)}`}
                    </List.Description>
                </List.Item>
            </List>
            <p
                style={{
                    wordWrap: 'break-word'
                }}
            >
                {item.body}
            </p>
            {item.file_keys && <Attachment keys={item.file_keys} />}
        </>
    )
}

//display attachments
const Attachment = props => {
    return (
        <Image.Group size="small">
            {props.keys.map(key => (
                <Image as="a" href={key} src={key}>
                    <a href={key}>
                        {
                            key
                                .split('/')
                                .pop()
                                .split('?')[0]
                        }
                    </a>
                </Image>
            ))}
        </Image.Group>
    )
}

class CommentForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            content: '',
            isCommented: false
        }
    }
    handleChange = (e, { name, value }) => this.setState({ [name]: value })
    handleSubmitComment = () => {
        if (!this.state.content) {
            return swal(m.CustomError('Your have not written anything'))
        }

        const item = this.props.item
        const endpoint = util.createEndpoint(`/feed/${item.id}/comment/new`)
        const request = new Request(endpoint, {
            method: 'POST',
            body: JSON.stringify({
                content: this.state.content
            }),
            headers: {
                'Content-Type': 'application/json',
                Authorization: util.getObject('token')
            }
        })
        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        swal(m.PostSuccess('comment'))
                        setTimeout(() => window.location.reload(), 5000)
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .catch(error => swal(m.NetworkError()))
    }
    render() {
        const likeCount = this.props.item.like_count
        const itemId = this.props.item.id

        return (
            <Form reply>
                <Form.TextArea
                    name="content"
                    value={this.state.content}
                    onChange={this.handleChange}
                    placeholder="Your comments..."
                />
                <Button
                    icon
                    labelPosition="left"
                    onClick={() => submitLike(itemId)}
                >
                    <Icon name="heart" />
                    {likeCount} likes
                </Button>
                <Button
                    content="Add Comment"
                    labelPosition="left"
                    icon="edit"
                    primary
                    onClick={this.handleSubmitComment}
                />
            </Form>
        )
    }
}

function submitLike(itemId) {
    const endpoint = util.createEndpoint(`/feed/${itemId}/like`)

    const request = new Request(endpoint, {
        method: 'POST',
        headers: {
            Authorization: util.getObject('token')
        }
    })

    fetch(request)
        .then(res => res.json())
        .then(result => {
            switch (result.status) {
                case 1:
                    result.is_cancel
                        ? swal(m.CustomSuccess('Your like has been cancelled'))
                        : swal(m.CustomSuccess('Your like has been submitted'))
                    setTimeout(() => window.location.reload(), 3000)
                    break
                default:
                    swal(m.ServerError(result))
            }
        })
        .catch(error => swal(m.NetworkError()))
}

export default withRouter(FeedPage)
