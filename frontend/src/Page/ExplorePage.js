import React, { Component } from 'react'
import {
    Header,
    Form,
    List,
    Dropdown,
    Segment,
    Button,
    Input,
    Menu
} from 'semantic-ui-react'
import ItemList from '../Component/ItemList'
import { withRouter } from 'react-router-dom'
import util from '../util'
import swal from 'sweetalert'
import * as m from '../msgDict'
import PageLoader from '../Component/PageLoader'

/*
 * Display a bunch of groups
 */

/*
export default function ExplorePage() {
    const { orgId } = useParams()
    return <ExplorePageComponent orgId={orgId} />
}
*/

class ExplorePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            inputValue: '',
            activeCategory: 'group',
            searchValue: '',
            organization: null,
            isLoading: true
        }
    }
    componentDidMount() {
        const orgId = this.props.match.params.orgId
        const endpoint = util.createEndpoint(`/organization/${orgId}`)
        const request = new Request(endpoint, {
            method: 'GET'
        })
        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        this.setState({ organization: result.organization })
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .then(() => this.setState({ isLoading: false }))
            .catch(error => swal(m.NetworkError()))
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    /* rerender ItemList component by setting new state */
    handleSearch() {
        this.setState({ searchValue: this.state.inputValue })
    }

    render() {
        const {
            isLoading,
            searchValue,
            inputValue,
            activeCategory,
            organization
        } = this.state
        const orgId = this.props.match.params.orgId
        const endpoint = searchValue
            ? util.createEndpoint('/search')
            : util.createEndpoint(`/organization/${orgId}/${activeCategory}`)

        if (isLoading) {
            return <PageLoader />
        }
        return (
            <>
                <Segment basic textAlign="center">
                    <Header
                        as="h1"
                        content={`Dive in ${organization.name}`}
                        subheader={organization.about}
                    />
                    <Form>
                        <Form.Input
                            style={{ maxWidth: '350px' }}
                            action={
                                <Button
                                    primary
                                    onClick={() => this.handleSearch()}
                                >
                                    Search
                                </Button>
                            }
                            name="inputValue"
                            icon="search"
                            iconPosition="left"
                            placeholder={`See what's in ${organization.name}...`}
                            onChange={this.handleChange}
                            value={inputValue}
                        />
                        <Form.Field>
                            <List size="big" link horizontal>
                                <List.Item
                                    active={'group' === activeCategory}
                                    as="a"
                                    onClick={() =>
                                        this.setState({
                                            activeCategory: 'group',
                                            inputValue: '',
                                            searchValue: ''
                                        })
                                    }
                                >
                                    Group
                                </List.Item>
                                <List.Item
                                    active={'user' === activeCategory}
                                    as="a"
                                    onClick={() =>
                                        this.setState({
                                            activeCategory: 'user',
                                            inputValue: '',
                                            searchValue: ''
                                        })
                                    }
                                >
                                    User
                                </List.Item>
                                <List.Item
                                    active={'feed' === activeCategory}
                                    as="a"
                                    onClick={() =>
                                        this.setState({
                                            activeCategory: 'feed',
                                            inputValue: '',
                                            searchValue: ''
                                        })
                                    }
                                >
                                    Feed
                                </List.Item>
                            </List>
                        </Form.Field>
                    </Form>
                </Segment>
                <ItemList
                    itemName={activeCategory}
                    searchValue={searchValue}
                    endpoint={endpoint}
                />
            </>
        )
    }
}

const FilterBar = () => (
    <Menu>
        <Menu.Item>
            <Input className="icon" icon="search" placeholder="Search..." />
        </Menu.Item>

        <Menu.Item position="right">
            <Input
                action={{ type: 'submit', content: 'Go' }}
                placeholder="Navigate to..."
            />
        </Menu.Item>
    </Menu>
)
export default withRouter(ExplorePage)
