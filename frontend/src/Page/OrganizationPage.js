import React, { Component } from 'react'
import { Segment } from 'semantic-ui-react'
import ItemList from '../Component/ItemList'
import util from '../util'

export default function OrganizationPage() {
    return <OrganizationPageComponent />
}
class OrganizationPageComponent extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const endpoint = util.createEndpoint('/organization')
        return <ItemList itemName="organization" endpoint={endpoint} />
    }
}
