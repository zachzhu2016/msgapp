import React, { Component } from 'react'
import {
    Segment,
    Modal,
    Grid,
    Image,
    List,
    Icon,
    Header,
    Button,
    Container
} from 'semantic-ui-react'
import HavingFunSvg from '../images/illustrations/havingfun.svg'
import TeamSvg from '../images/illustrations/team.svg'
import TeamSpiritSvg from '../images/illustrations/teamspirit.svg'
import GroupChatSvg from '../images/illustrations/groupchat.svg'

export default function HomePage() {
    return (
        <>
            <Segment textAlign="center" style={{ padding: '1em 0em' }} basic>
                <HomePageHeading />
            </Segment>
        </>
    )
}

// remove inline styling
const HomePageHeading = ({ mobile }) => (
    <Container text>
        <Header
            as="h1"
            content="Welcome"
            style={{
                fontSize: mobile ? '2em' : '4em',
                fontWeight: 'normal',
                marginBottom: 0,
                marginTop: mobile ? '1.5em' : '3em'
            }}
        />
        <Header
            as="h1"
            content="A discussion platform crafted for all of your groups."
            subheader="Useful for course discussions, group projects, organizing events..."
            style={{
                fontSize: mobile ? '1.5em' : '1.7em',
                fontWeight: 'normal',
                marginTop: mobile ? '0.5em' : '1.5em'
            }}
        />
        <HowItWorksModal />
    </Container>
)
class HowItWorksModal extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <Modal
                size="large"
                dimmer="blurring"
                trigger={
                    <Button primary size="huge">
                        How it works
                        <Icon name="right arrow" />
                    </Button>
                }
            >
                <Modal.Content scrolling>
                    <Modal.Description>
                        <NewHomePageBody />
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        )
    }
}

const NewHomePageBody = () => (
    <Grid padded={true}>
        <Grid.Row>
            <Grid.Column mobile={16} tablet={8} computer={4}>
                <Segment basic>
                    <Header
                        textAlign="center"
                        as="h2"
                        content="Sign In"
                        subheader="Sign in with one of your Google accounts corresponding to the organization you desire to be part of. No sign up needed."
                    />
                </Segment>
            </Grid.Column>
            <Grid.Column mobile={16} tablet={8} computer={4}>
                <Segment basic>
                    <Header
                        textAlign="center"
                        as="h2"
                        content="Create Groups"
                        subheader="You can either create public groups where all feeds are visible to the public or private groups where only the group members can read and write."
                    />
                </Segment>
            </Grid.Column>
            <Grid.Column mobile={16} tablet={8} computer={4}>
                <Segment basic>
                    <Header
                        textAlign="center"
                        as="h2"
                        content="Invite Others"
                        subheader="Invite others to your group by distributing our secure invitation link, which automatically bring thme into the group."
                    />
                </Segment>
            </Grid.Column>
            <Grid.Column mobile={16} tablet={8} computer={4}>
                <Segment basic>
                    <Header
                        textAlign="center"
                        as="h2"
                        content="Have Fun"
                        subheader="Now you can discuss topics, ask questions, share resources, and make plans in your own world shared with others"
                    />
                </Segment>
            </Grid.Column>
        </Grid.Row>
    </Grid>
)

const HomePageBody = ({ mobile }) => (
    <>
        <Segment style={{ padding: '8em 0em' }} basic>
            <Grid container stackable verticalAlign="middle">
                <Grid.Row>
                    <Grid.Column width={8}>
                        <Header as="h3" style={{ fontSize: '2em' }}>
                            Sign in with your Google account
                        </Header>
                        <p style={{ fontSize: '1.33em' }}>
                            We authenticate your identity by comparing your
                            logged-in Email domain with that of the
                            organization. You can only <b>post</b> feeds within
                            your organization and <b>view</b> all public feeds /
                            groups across organizations.
                        </p>
                    </Grid.Column>
                    <Grid.Column floated="right" width={6}>
                        <Image rounded size="large" src={GroupChatSvg} />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
        <Segment style={{ padding: '8em 0em' }} basic>
            <Grid container stackable verticalAlign="middle">
                <Grid.Row>
                    <Grid.Column floated="left" width={6}>
                        <Image rounded size="large" src={TeamSvg} />
                    </Grid.Column>
                    <Grid.Column width={8}>
                        <Header as="h3" style={{ fontSize: '2em' }}>
                            Create your own groups
                        </Header>
                        <p style={{ fontSize: '1.33em' }}>
                            You can either create <b>public groups</b> where all
                            feeds are visible to public or <b>private groups</b>{' '}
                            where only the group members can view and post
                            feeds.
                        </p>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
        <Segment style={{ padding: '8em 0em' }} basic>
            <Grid container stackable verticalAlign="middle">
                <Grid.Row>
                    <Grid.Column width={8}>
                        <Header as="h3" style={{ fontSize: '2em' }}>
                            Invite your friends / classmates
                        </Header>
                        <p style={{ fontSize: '1.33em' }}>
                            Once you have created or joined a group, you can
                            invite more people to join. The invited ones simply
                            need to click on the invitation link and they are
                            in!
                        </p>
                    </Grid.Column>
                    <Grid.Column floated="right" width={6}>
                        <Image rounded size="large" src={TeamSpiritSvg} />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
        <Segment style={{ padding: '8em 0em' }} basic>
            <Grid container stackable verticalAlign="middle">
                <Grid.Row>
                    <Grid.Column floated="left" width={6}>
                        <Image rounded size="large" src={HavingFunSvg} />
                    </Grid.Column>
                    <Grid.Column width={8}>
                        <Header as="h3" style={{ fontSize: '2em' }}>
                            Discuss, ask, and share
                        </Header>
                        <p style={{ fontSize: '1.33em' }}>
                            A group could be for a course, a club, a project, a
                            shared interest...
                        </p>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
    </>
)

/*
HomePageHeading.propTypes = {
    mobile: PropTypes.bool
}
*/
