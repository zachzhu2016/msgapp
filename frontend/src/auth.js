import { createBrowserHistory } from 'history'
const history = createBrowserHistory()
const auth = {
    signin(result, callback) {
        if (result.user) {
            window.localStorage.setItem('user', JSON.stringify(result.user))
            window.localStorage.setItem('token', result.token)
            history.push('/home')
            window.location.reload()
        } else {
            alert('Failed to authenticate user.')
        }
        setTimeout(callback, 100)
    },
    storeUser(user) {
        window.localStorage.setItem('user', JSON.stringify(user))
    },
    signout(callback) {
        window.localStorage.removeItem('user')
        window.localStorage.removeItem('token')
        setTimeout(callback, 100)
        history.push('/login')
        window.location.reload()
    },
    getObject(name, callback) {
        let obj = window.localStorage.getItem(name)
        if (name === 'user') {
            obj = JSON.parse(obj)
        }
        setTimeout(callback, 100)
        return obj ? obj : ''
    },
    timeoutCallback() {
        alert('Time out. Please sign in again.')
    }
}

export default auth
