import React, { Component } from 'react'
import util from '../util'
import FlexList from '../Component/FlexList'
import {
    Message,
    Grid,
    Input,
    Divider,
    Modal,
    Dropdown,
    Icon,
    Popup,
    Form,
    Header,
    Button
} from 'semantic-ui-react'
import swal from 'sweetalert'
import * as m from '../msgDict'

export default class InviteModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            emailList: [],
            shareLink: '',
            inviteScope: 'anyone',
            copySuccess: '',
            email: ''
        }
        this.handleRemove = this.handleRemove.bind(this)
    }

    handleAddEmail = () => {
        if (
            !this.state.emailList.includes(this.state.email) &&
            this.state.email
        ) {
            this.setState({
                emailList: [...this.state.emailList, this.state.email]
            })
        }
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    handleRemove = removedEmail => {
        this.setState(prevState => ({
            emailList: prevState.emailList.filter(
                email => email !== removedEmail
            )
        }))
    }

    handleCreateLink = () => {
        const groupId = this.props.groupId
        const { inviteScope, emailList } = this.state
        if (inviteScope === 'listed' && !emailList.length) {
            return swal(
                m.CustomInfo('You must have at least one email address.')
            )
        }
        const endpoint = util.createEndpoint(
            `/group/${groupId}/invitation/send?scope=${inviteScope}`
        )
        const request = new Request(endpoint, {
            method: 'POST',
            body: JSON.stringify({
                email_list: emailList
            }),
            headers: {
                'Content-Type': 'application/json',
                Authorization: util.getObject('token')
            }
        })
        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        this.setState({
                            shareLink: result.share_link
                        })
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .catch(error => swal(m.NetworkError()))
    }

    copyToClipboard = e => {
        this.input.select()
        document.execCommand('copy')
        e.target.focus()
        this.setState({ copySuccess: 'Copied!' })
    }

    handleSendInvite = () => {
        const { emailList } = this.state
        const groupId = this.props.groupId
        const endpoint = util.createEndpoint(
            `/group/${groupId}/invitation/send?scope=listed`
        )

        const request = new Request(endpoint, {
            method: 'POST',
            body: JSON.stringify({
                email_list: emailList
            }),
            headers: {
                'Content-Type': 'application/json',
                Authorization: util.getObject('token')
            }
        })

        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        swal(m.CustomSuccess('Invitations have been sent.'))
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .catch(error => swal(m.NetworkError()))
    }

    formReset = () => {
        this.setState({
            emailList: [],
            shareLink: '',
            inviteScope: 'anyone',
            copySuccess: '',
            email: ''
        })
    }

    render() {
        const {
            email,
            emailList,
            inviteScope,
            copySuccess,
            shareLink
        } = this.state
        return (
            <Modal
                size="tiny"
                trigger={<Button content="Invite" compact primary tiny />}
                onClose={this.formReset}
            >
                <Modal.Content>
                    <Modal.Description>
                        <Header>
                            <Divider horizontal>
                                <Header as="h4">
                                    <Icon name="group" />
                                    Invitee List
                                </Header>
                            </Divider>
                        </Header>
                        {emailList.length ? (
                            <FlexList
                                itemList={emailList}
                                handleRemove={this.handleRemove}
                            />
                        ) : (
                            <Message
                                icon="inbox"
                                header=""
                                content="No one has been added to the list."
                            />
                        )}
                        <Form>
                            <Form.Input
                                placeholder="Email"
                                name="email"
                                fluid
                                value={email}
                                onChange={this.handleChange}
                            />
                            <Form.Group>
                                <Form.Button
                                    onClick={this.handleAddEmail}
                                    content="Add Email"
                                />
                                <Form.Button
                                    type="submit"
                                    floated="right"
                                    primary
                                    onClick={this.handleSendInvite}
                                    content="Send Invitations"
                                />
                            </Form.Group>
                            <Form.Field>
                                <Divider horizontal>Or</Divider>
                            </Form.Field>
                            <Form.Field>
                                <Form.Button
                                    primary
                                    onClick={this.handleCreateLink}
                                    content="Create Shareable Link"
                                />
                            </Form.Field>
                            <Form.Field>
                                <span>
                                    {'Make it shareable to '}
                                    <Dropdown
                                        inline
                                        name="inviteScope"
                                        options={[
                                            {
                                                key: 'anyone',
                                                text: 'anyone with the link',
                                                value: 'anyone'
                                            },
                                            {
                                                key: 'invitee list',
                                                text: 'invitee list only',
                                                value: 'listed'
                                            }
                                        ]}
                                        value={inviteScope}
                                        onChange={this.handleChange}
                                    />{' '}
                                </span>
                            </Form.Field>
                            {shareLink && (
                                <>
                                    <Form.Field>
                                        <Popup
                                            content={copySuccess}
                                            open={copySuccess ? true : false}
                                            position="top center"
                                            trigger={
                                                <Input
                                                    action={{
                                                        labelPosition: 'right',
                                                        icon: 'copy',
                                                        content: 'Copy',
                                                        onClick: this
                                                            .copyToClipboard
                                                    }}
                                                    ref={input =>
                                                        (this.input = input)
                                                    }
                                                    value={shareLink}
                                                />
                                            }
                                        />
                                    </Form.Field>
                                    <Message warning visible>
                                        <p>
                                            This link will expire after 24
                                            hours.
                                        </p>
                                    </Message>
                                </>
                            )}
                        </Form>
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        )
    }
}
