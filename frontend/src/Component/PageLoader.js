import { Grid, Dimmer, Loader } from 'semantic-ui-react'
import React, { Component } from 'react'

function PageLoader(props) {
    return (
        <Dimmer active inverted>
            <Loader inverted>{props.children}</Loader>
        </Dimmer>
    )
}

export default PageLoader
