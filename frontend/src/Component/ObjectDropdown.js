import _ from 'lodash'
import React, { Component } from 'react'
import { Dropdown, Header, Icon } from 'semantic-ui-react'
import auth from '../auth'
import swal from 'sweetalert'
import * as m from '../msgDict'

/*
 * required props: objectName, attr
 *
 * */
export default class ObjectDropdown extends Component {
    constructor(props) {
        super(props)
        this.state = {
            objectList: []
        }
    }
    componentDidMount() {
        let endpoint = ''

        switch (this.props.objectName) {
            case 'group':
                endpoint = 'https://api.groupible.com/group?view=mine'
                break
            case 'feed':
                endpoint = 'https://api.groupible.com/feed?view=mine'
                break
            default:
                this.setState({ error: 'Invalid endpoint' })
        }

        const request = new Request(endpoint, {
            method: 'GET',
            headers: {
                Authorization: auth.getToken()
            }
        })

        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        let objectList = []
                        const attr = this.props.attr
                        Object.keys(result).forEach(function(key, index) {
                            if (key == 'feeds' || key == 'groups') {
                                for (const object of result[key]) {
                                    objectList.push({
                                        key: object.id,
                                        text: object[attr],
                                        value: object[attr]
                                    })
                                }
                            }
                        })
                        this.setState({ objectList: objectList })
                        break
                    default:
                        swal(m.ServerError(result))
                }
            })
            .catch(error => swal(m.NetworkError))
    }
    render() {
        const objectList = this.state.objectList
        const objectName = this.props.objectName
        return (
            <Dropdown
                inline
                placeholder={objectList[0] ? objectList[0].text : ''}
                search
                options={objectList}
            />
        )
    }
}
