import React, { Component } from 'react'
import {
    Icon,
    Checkbox,
    Modal,
    Segment,
    Grid,
    Divider,
    Header,
    Button,
    Form,
    TextArea
} from 'semantic-ui-react'
import ObjectSearchBox from '../Component/ObjectSearchBox'
import auth from '../auth'
import swal from 'sweetalert'
import * as m from '../msgDict'

export default function GroupBanner(props) {
    return (
        <Segment placeholder>
            <Grid columns={2} stackable textAlign="center">
                <Divider vertical>Or</Divider>

                <Grid.Row verticalAlign="middle">
                    <Grid.Column>
                        <Header icon>
                            <Icon name="search" />
                            Find Group
                        </Header>
                        <ObjectSearchBox objectName="group" matchAttr="name" />
                    </Grid.Column>

                    <Grid.Column>
                        <Header icon>
                            <Icon name="group" />
                            Create a New Group
                        </Header>
                        <NewGroupFormModal />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
    )
}

class NewGroupFormModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            about: '',
            isPublic: false,
            showModal: false
        }
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    toggle = () =>
        this.setState(prevState => ({ isPublic: !prevState.isPublic }))

    handleSubmit = () => {
        const { name, about, isPublic } = this.state
        if (!name || !about) {
            return swal(m.ArgumentError)
        }

        const endpoint = `https://localhost:5000/group/new`
        const request = new Request(endpoint, {
            method: 'POST',
            body: JSON.stringify({
                name: name,
                about: about,
                is_public: isPublic & 1
            }),
            headers: {
                'Content-Type': 'application/json',
                Authorization: auth.getObject('token')
            }
        })
        fetch(request)
            .then(res => res.json())
            .then(
                result => {
                    switch (result.status) {
                        case 1:
                            alert(
                                `You have successfully created ${name} as your new group`
                            )
                            this.closeModal()
                            window.location.reload()
                            break
                        default:
                            alert(result.error)
                    }
                },
                error => {
                    alert(error)
                }
            )
    }

    closeModal = () => {
        this.setState({ showModal: false })
    }

    render() {
        console.log(JSON.stringify(this.state))
        const { name, about, showModal } = this.state
        return (
            <Modal
                open={showModal}
                centered={false}
                trigger={
                    <Button
                        primary
                        onClick={() => this.setState({ showModal: true })}
                    >
                        Create
                    </Button>
                }
            >
                <Modal.Header>{'Create a new group'}</Modal.Header>
                <Modal.Content image>
                    <Modal.Description>
                        <Form>
                            <Form.Input
                                label="Name"
                                name="name"
                                placeholder="Give your group a name..."
                                value={name}
                                onChange={this.handleChange}
                            />
                            <Form.Input
                                control={TextArea}
                                label="About"
                                name="about"
                                placeholder="Say something about this group..."
                                value={about}
                                onChange={this.handleChange}
                            />
                            <Checkbox
                                label="Make it a public group."
                                onChange={this.toggle}
                                checked={this.state.isPublic}
                            />
                        </Form>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button onClick={this.closeModal}>Cancel</Button>
                    <Button primary onClick={this.handleSubmit}>
                        Submit
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}
