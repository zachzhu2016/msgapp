import faker from 'faker'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
    List,
    Pagination,
    Button,
    Dimmer,
    Container,
    Comment,
    Loader,
    Segment,
    Image,
    Placeholder,
    Label,
    Message,
    Feed,
    Icon,
    Card,
    Popup,
    Item,
    Header
} from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import util from '../util'
import swal from 'sweetalert'
import * as m from '../msgDict'
import defaultProfpic from '../images/defaultProfpic.png'

export default class ItemList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            itemList: [],
            isLoading: true,
            itemName: this.props.itemName,
            searchValue: this.props.searchValue ? this.props.searchValue : '',
            // pagination
            activePage: 1,
            boundaryRange: 1,
            siblingRange: 1,
            showEllipsis: true,
            showFirstAndLastNav: false,
            showPreviousAndNextNav: true,
            totalPages: 0,
            pageSize: 16
        }
    }

    handlePaginationChange = (e, { activePage }) =>
        this.setState({ activePage }, () => this.fetchList())

    componentDidMount() {
        this.fetchList()
    }

    componentDidUpdate() {
        if (this.props.itemName !== this.state.itemName) {
            this.fetchList('itemName')
        } else if (
            this.props.searchValue !== this.state.searchValue &&
            this.props.searchValue !== ''
        ) {
            this.fetchList('searchValue')
        }
    }

    fetchList(changedParam) {
        const { pageSize } = this.state
        const activePage =
            changedParam !== 'itemName' ? this.state.activePage : 1
        const { itemName, searchValue } = this.props // update from upstream
        let endpoint = this.props.endpoint

        const param = searchValue
            ? `objName=${itemName}&q=${searchValue}`
            : `page=${activePage - 1}`
        endpoint = endpoint.includes('?')
            ? `${endpoint}&${param}`
            : `${endpoint}?${param}`

        const request = new Request(endpoint, {
            method: 'GET',
            headers: {
                Authorization: util.getObject('token')
            }
        })

        var itemList = []

        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        Object.keys(result).forEach(function(key, index) {
                            if (
                                key === 'feeds' ||
                                key === 'groups' ||
                                key === 'users' ||
                                key === 'comments' ||
                                key === 'organizations' ||
                                key === 'files'
                            ) {
                                itemList = result[key]
                            }
                        })
                        this.setState({
                            itemList: itemList,
                            isLoading: false,
                            itemName: itemName,
                            searchValue: searchValue,
                            activePage: activePage,
                            totalPages: Math.ceil(result.total / pageSize)
                        })
                        break
                    default:
                        swal(m.ServerError(result))
                        this.setState({
                            searchValue: searchValue,
                            itemName: itemName
                        })
                }
            })
            .then(() => window.scrollTo({ top: 0, behavior: 'smooth' }))
            .catch(error => swal(m.NetworkError()))
    }

    render() {
        const {
            activePage,
            boundaryRange,
            siblingRange,
            showEllipsis,
            showFirstAndLastNav,
            showPreviousAndNextNav,
            totalPages,
            endpoint,
            isLoading,
            itemList
        } = this.state
        const { itemName, searchValue } = this.props

        if (isLoading) {
            return <MessageLoader />
        } else if (!itemList.length) {
            return (
                <Message>
                    <Message.Header>Search result empty.</Message.Header>
                </Message>
            )
        } else {
            const ItemListPagination = (
                <Segment textAlign="center" basic>
                    <Pagination
                        activePage={activePage}
                        boundaryRange={boundaryRange}
                        onPageChange={this.handlePaginationChange}
                        size="small"
                        siblingRange={siblingRange}
                        totalPages={totalPages}
                        // Heads up! All items are powered by shorthands, if you want to hide one of them, just pass `null` as value
                        ellipsisItem={showEllipsis ? undefined : null}
                        firstItem={showFirstAndLastNav ? undefined : null}
                        lastItem={showFirstAndLastNav ? undefined : null}
                        prevItem={showPreviousAndNextNav ? undefined : null}
                        nextItem={showPreviousAndNextNav ? undefined : null}
                    />
                </Segment>
            )
            switch (itemName) {
                case 'organization':
                    return (
                        <Segment vertical>
                            <Card.Group stackable doubling itemsPerRow={4}>
                                {itemList.map(item => (
                                    <OrganizationCardItem
                                        key={item.id}
                                        item={item}
                                    />
                                ))}
                                <SignUpCardItem />
                            </Card.Group>

                            {totalPages > 1 ? ItemListPagination : ''}
                        </Segment>
                    )
                case 'feed':
                    return (
                        <Segment vertical>
                            <Card.Group stackable doubling itemsPerRow={4}>
                                {itemList.map(item => (
                                    <FeedCardItem key={item.id} item={item} />
                                ))}
                            </Card.Group>
                            {totalPages > 1 ? ItemListPagination : ''}
                        </Segment>
                    )
                case 'myFeed':
                    return (
                        <Item.Group divided>
                            {itemList.map(item => (
                                <MyFeedBlockItem key={item.id} item={item} />
                            ))}
                            {totalPages > 1 ? ItemListPagination : ''}
                        </Item.Group>
                    )
                case 'group':
                    return (
                        <Segment vertical>
                            <Card.Group stackable doubling itemsPerRow={4}>
                                {itemList.map(item => (
                                    <GroupCardItem key={item.id} item={item} />
                                ))}
                            </Card.Group>
                            {totalPages > 1 ? ItemListPagination : ''}
                        </Segment>
                    )
                case 'user':
                    return (
                        <Segment vertical>
                            <Card.Group stackable doubling itemsPerRow={4}>
                                {itemList.map(item => (
                                    <UserCardItem key={item.id} item={item} />
                                ))}
                            </Card.Group>
                            {totalPages > 1 ? ItemListPagination : ''}
                        </Segment>
                    )
                case 'comment':
                    return (
                        <Segment vertical>
                            {itemList.map(item => (
                                <CommentItem key={item.id} item={item} />
                            ))}
                            {totalPages > 1 ? ItemListPagination : ''}
                        </Segment>
                    )
                case 'file':
                    return (
                        <Segment vertical>
                            <List
                                divided
                                relaxed
                                verticalAlign="middle"
                                size="large"
                            >
                                {itemList.map(item => (
                                    <FileItem key={item.id} item={item} />
                                ))}
                            </List>
                            {totalPages > 1 ? ItemListPagination : ''}
                        </Segment>
                    )

                default:
            }
        }
    }
}

/* props requirements */
ItemList.contextTypes = {
    itemName: PropTypes.object.isRequired,
    endpoint: PropTypes.object.isRequired
}

ItemList.defaultProps = {
    searchValue: ''
}

const OrganizationCardItem = props => {
    const org = props.item
    const orgPicKey = util.createFileUrl(org.profpic_key)
    const orgId = org.id
    const orgName = org.name ? org.name : ''
    const orgAbout = org.about ? org.about : ''
    const orgDomain = org.domain ? org.domain : ''
    const orgLocation = `${org ? org.city : ''}, ${org ? org.state : ''}`

    return (
        <Card
            as={Link}
            to={location => `${location.pathname}/${orgId}/explore`}
        >
            <Image src={orgPicKey} wrapped ui={false} />
            <Card.Content>
                <Card.Header>{orgName}</Card.Header>
                <Card.Meta>
                    <span className="date">{orgLocation}</span>
                </Card.Meta>
                <Card.Description>{orgAbout}</Card.Description>
            </Card.Content>
            <Card.Content extra>{orgDomain}</Card.Content>
        </Card>
    )
}

const SignUpCardItem = props => (
    <Card>
        <Card.Content>
            <Card.Header>Join our family</Card.Header>
            <Card.Meta>
                <span className="date">Sign up an organization</span>
            </Card.Meta>
            <Card.Description>
                Send an email to{' '}
                <a href="mailto:support@groupible.com">support@groupible.com</a>{' '}
                to submit your request. We will get back to you within 3 days.
            </Card.Description>
        </Card.Content>
        <Card.Content extra>groupible.com</Card.Content>
    </Card>
)

const FeedCardItem = props => {
    const feed = props.item
    const feedId = feed.id
    const body = feed.body ? feed.body.substring(0, 200) : ''
    const showMore =
        body.length > 200 ? <a href={`/feed/${feed.id}`}>view more</a> : ''
    const timeDiff = util.displayTimeDiff(feed.created)
    const fullname = feed.author
        ? `${feed.author.firstname} ${feed.author.lastname}`
        : ''
    const title = feed.title ? feed.title.substring(0, 50) : ''
    const likeCount = feed.like_count
    const commentCount = feed.comment_count
    const keyNum = feed.file_keys ? feed.file_keys.length : 0
    const feedLabel = feedLabelMaker(feed.tag)

    return (
        <Popup
            content="Popup will hide in 500ms after leaving mouse."
            mouseEnterDelay={1000}
            on="hover"
            hoverable
            position="top center"
            trigger={
                <Card as={Link} to={`/feed/${feed.id}`}>
                    <Card.Content>
                        <Card.Header>{feedLabel}</Card.Header>
                        <Card.Meta>
                            {fullname} {timeDiff}
                        </Card.Meta>
                    </Card.Content>
                    <Card.Content>
                        <Card.Description>{title}</Card.Description>
                    </Card.Content>
                    <Button.Group attached="bottom">
                        <Button>
                            <Icon name="heart" />
                            {likeCount} likes
                        </Button>
                        <Button>
                            <Icon name="comment" />
                            {commentCount} comments
                        </Button>
                    </Button.Group>
                    {/*<Label.Group>
                    <Label as="a" onClick={() => submitLike(feed.id)}>
                        <Icon name="heart" />
                        {feed.like_count}
                        <Label.Detail>like</Label.Detail>
                    </Label>
                    <Label>
                        <Icon name="comment" />
                        {feed.comment_count}
                        <Label.Detail>comment</Label.Detail>
                    </Label>
                </Label.Group>*/}
                </Card>
            }
        >
            <React.Fragment>
                <Header
                    as="h2"
                    content={title}
                    subheader={`${keyNum} attachments`}
                />
                <p
                    style={{
                        maxWidth: '400px',
                        wordWrap: 'break-word'
                    }}
                >
                    {body}
                </p>
                {showMore}
            </React.Fragment>
        </Popup>
    )
}

const MyFeedBlockItem = props => {
    const feed = props.item
    const feedId = feed.id
    const body = feed.body ? feed.body.substring(0, 200) : ''
    const showMore =
        body.length > 200 ? <a href={`/feed/${feed.id}`}>view more</a> : ''
    const timeDiff = util.displayTimeDiff(feed.created)
    const fullname = feed.author
        ? `${feed.author.firstname} ${feed.author.lastname}`
        : ''
    const title = feed.title ? feed.title.substring(0, 50) : ''
    const likeCount = feed.like_count
    const commentCount = feed.comment_count
    const keyNum = feed.file_keys ? feed.file_keys.length : 0
    const feedLabel = feedLabelMaker(feed.tag)

    return (
        <Item>
            <Item.Content>
                <Item.Header as={Link} to={`/feed/${feed.id}`}>
                    {feedLabel}
                </Item.Header>
                <Item.Meta>
                    <span className="cinema">
                        {fullname} {timeDiff}
                    </span>
                </Item.Meta>
                <Item.Description>{body}</Item.Description>
                <Item.Extra>
                    <Button
                        floated="right"
                        onClick={() => {
                            if (
                                window.confirm(
                                    'Do you really want to delete this item?'
                                )
                            ) {
                                execMyFeedAction(feedId, 'delete')
                            }
                        }}
                    >
                        Delete
                        <Icon name="right close" />
                    </Button>
                    {/*
                    <Button primary floated="right">
                        Edit
                        <Icon name="right edit" />
                    </Button>
                    */}
                    <Label>
                        <Icon name="heart" />
                        {likeCount} likes
                    </Label>
                    <Label>
                        <Icon name="comment" />
                        {commentCount} comments
                    </Label>
                </Item.Extra>
            </Item.Content>
        </Item>
    )
}

function execMyFeedAction(feedId, action) {
    if (action !== 'delete') {
        return swal(m.CustomError('Cannot execute this action.'))
    }
    const endpoint = util.createEndpoint(`/feed/${feedId}/${action}`)
    const request = new Request(endpoint, {
        method: action === 'delete' ? 'DELETE' : 'POST',
        headers: {
            Authorization: util.getObject('token')
        }
    })
    fetch(request)
        .then(res => res.json())
        .then(result => {
            switch (result.status) {
                case 1:
                    swal(m.DeleteItemSuccess()).then(() =>
                        window.location.reload()
                    )
                    break
                default:
                    swal(m.ServerError(result))
            }
        })
        .catch(error => {
            swal(m.NetworkError())
        })
}

// returns appropriate UI label for a tag value
function feedLabelMaker(tag) {
    let label = null
    switch (tag) {
        case 'discuss':
            label = (
                <>
                    <Icon name="discussions" />
                    Discuss
                </>
            )
            break
        case 'question':
            label = (
                <>
                    <Icon name="question" />
                    Question
                </>
            )
            break
        case 'share':
            label = (
                <>
                    <Icon name="share" />
                    Sharing
                </>
            )
            break
        case 'notice':
            label = (
                <>
                    <Icon name="announcement" />
                    Announcement
                </>
            )
            break
        default:
    }
    return label
}

const UserCardItem = props => {
    const user = props.item
    const userId = user.id
    const fullname = ` ${user.firstname} ${user.lastname}`
    const userEmail = user.email ? user.email : ''
    const userAbout = user.about
        ? user.about.length > 80
            ? user.about.substring(0, 80) + '... (more)'
            : ''
        : ''

    return (
        <Card as={Link} to={`/user/${userId}`}>
            <Card.Content>
                <Card.Header>
                    <Icon name="user" />
                    {fullname}
                </Card.Header>
                <Card.Meta>{userEmail}</Card.Meta>
                <Card.Description>{userAbout}</Card.Description>
            </Card.Content>
        </Card>
    )
}
const GroupCardItem = props => {
    const group = props.item
    const groupId = group.id
    const groupAbout = group.about
        ? group.about.substring(0, 200) +
          (group.about.length > 200 ? '... (more)' : '')
        : ''
    const groupType = group.is_public ? 'public' : 'private'
    const groupName = group.name

    return (
        <Card as={Link} to={`/group/${groupId}`}>
            <Card.Content>
                <Card.Header>
                    <Icon name="group" />
                    {' ' + groupName}
                </Card.Header>
                <Card.Meta>{groupType}</Card.Meta>
                <Card.Description>{groupAbout}</Card.Description>
            </Card.Content>
        </Card>
    )
}

const CommentItem = props => {
    const comment = props.item
    const fullname = comment.author
        ? `${comment.author.firstname} ${comment.author.lastname}`
        : ''
    const commentContent = comment.content ? comment.content : ''
    const userId = comment.user_id
    const timeDiff = util.displayTimeDiff(comment.created)

    return (
        <Comment>
            {/*
            <Comment.Avatar>
                <Image
                    avatar
                    src={
                        comment.author.profpic_key
                            ? util.createFileUrl(comment.author.profpic_key)
                            : defaultProfpic
                    }
                />
            </Comment.Avatar>
        */}
            <Comment.Content>
                <Comment.Author as={Link} to={`/user/${userId}`}>
                    {fullname}
                </Comment.Author>
                <Comment.Metadata>
                    <div>{timeDiff}</div>
                </Comment.Metadata>
                <Comment.Text>{commentContent}</Comment.Text>
            </Comment.Content>
        </Comment>
    )
}

const FileItem = props => {
    const feed = props.item
    const feedTitle = feed.title
    const feedId = feed.id
    const fullname = feed.author
        ? `${feed.author.firstname} ${feed.author.lastname} `
        : ''
    const hasFiles = feed.file_keys ? feed.file_keys.length > 0 : false
    const fileKeys = feed.file_keys ? feed.file_keys : []

    function getFileName(key) {
        return key
            .split('/')
            .pop()
            .split('?')[0]
    }

    return (
        <List.Item>
            <List.Content>
                <List.Header as={Link} to={`/feed/${feedId}`}>
                    {feedTitle}
                </List.Header>
                <List.Description>
                    by {fullname}
                    {util.displayTimeDiff(feed.created)}
                </List.Description>
                <List.List>
                    {hasFiles
                        ? feed.file_keys.map(key => (
                              <List.Item>
                                  <List.Icon name="file outline" />
                                  <List.Content>
                                      <List.Header as="a" href={key}>
                                          {getFileName(key)}
                                      </List.Header>
                                      <List.Description></List.Description>
                                  </List.Content>
                              </List.Item>
                          ))
                        : 'No files attached.'}
                </List.List>
            </List.Content>
        </List.Item>
    )
}

const MessageLoader = () => (
    <Message icon>
        <Icon name="circle notched" loading />
        <Message.Content>
            <Message.Header>Just one second</Message.Header>
            We are fetching that content for you.
        </Message.Content>
    </Message>
)
/*
const FeedItem = props => {
    const item = props.item
    return (
        <Feed.Event>
            <Feed.Content>
                <Feed.Summary>
                    <Feed.User as={Link} to={`/feed/${item.id}`}>
                        {item.title}
                    </Feed.User>
                    <Feed.Date>
                        posted on {item.created.substring(0, 10)}
                    </Feed.Date>
                </Feed.Summary>
                <Feed.Extra text>{item.content}</Feed.Extra>
                <Feed.Meta>
                    <Feed.Like onClick={() => submitVote(1, item.id)}>
                        <Icon name="thumbs up outline" />
                        {item.upvote_count} Upvote
                    </Feed.Like>
                    <Feed.Like onClick={() => submitVote(2, item.id)}>
                        <Icon name="thumbs down outline" />
                        {item.downvote_count} Downvote
                    </Feed.Like>
                    <Feed.Meta>
                        <Icon name="comment" />
                        {item.comment_count} comments
                    </Feed.Meta>
                    <EditModal item={item} />
                </Feed.Meta>
            </Feed.Content>
        </Feed.Event>
    )
}
*/
