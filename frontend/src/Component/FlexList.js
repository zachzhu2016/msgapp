import React, { Component } from 'react'
import { List, Button } from 'semantic-ui-react'
import util from '../util'

export default class FlexList extends Component {
    handleRemove(identifier) {
        this.props.handleRemove(identifier)
    }
    render() {
        let { itemList, identifier, extraProp } = this.props
        return (
            <List divided verticalAlign="middle">
                {itemList.map((item, index) => (
                    <List.Item key={index}>
                        <List.Content floated="right">
                            <Button
                                compact
                                onClick={this.handleRemove.bind(
                                    this,
                                    identifier ? item[identifier] : item
                                )}
                            >
                                remove
                            </Button>
                        </List.Content>
                        <List.Content>
                            <List.Header as="a">
                                {identifier ? item[identifier] : item}
                            </List.Header>
                            <List.Description>
                                {extraProp === 'size'
                                    ? util.displayFileSize(item[extraProp])
                                    : extraProp
                                    ? item[extraProp]
                                    : ''}
                            </List.Description>
                        </List.Content>
                    </List.Item>
                ))}
            </List>
        )
    }
}
