import _ from 'lodash'
import React, { Component } from 'react'
import { Search, Grid, Header, Segment } from 'semantic-ui-react'
import util from '../util'

/*
 * takes in an Object Name and a Matching Attribute
 * required props: objectName, matchAttr
 *
 * */
export default class ObjectSearchBox extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            initialList: [],
            changingList: [],
            dropdownList: [],
            error: '',
            value: ''
        }
    }

    componentDidMount() {
        let endpoint = ''
        switch (this.props.objectName) {
            case 'group':
                endpoint = util.createEndpoint('/group')
                break
            case 'feed':
                endpoint = util.createEndpoint('/feed')
                break
            case 'user':
                endpoint = util.createEndpoint('/user')
                break
            default:
                this.setState({ error: 'Invalid endpoint' })
        }

        const request = new Request(endpoint, {
            method: 'GET',
            headers: {
                Authorization: util.getObject('token')
            }
        })

        fetch(request)
            .then(res => res.json())
            .then(result => {
                switch (result.status) {
                    case 1:
                        let initialList = []
                        const matchAttr = this.props.matchAttr
                        Object.keys(result).forEach(function(key, index) {
                            if (
                                key == 'feeds' ||
                                key == 'groups' ||
                                key == 'users'
                            ) {
                                for (const object of result[key]) {
                                    initialList.push({
                                        title: object[matchAttr]
                                    })
                                }
                            }
                        })
                        this.setState({ initialList: initialList })
                        break
                    case 10:
                        util.signout(util.timeoutCallback())
                        break
                    default:
                        alert(result.error)
                }
            })
    }

    handleResultSelect = (e, { result }) =>
        this.setState({ value: result.title })

    handleSearchChange = (e, { value }) => {
        this.setState({ isLoading: true, value: value })

        setTimeout(() => {
            // if (this.state.value.length < 1) return this.setState(initialState);

            const re = new RegExp(_.escapeRegExp(this.state.value), 'i')
            const isMatch = result => re.test(result.title)

            this.setState({
                isLoading: false,
                changingList: _.filter(this.state.initialList, isMatch)
            })
        }, 300)
    }

    render() {
        const { isLoading, value, changingList, error } = this.state
        if (error) {
            alert(error)
        }
        return (
            <Search
                loading={isLoading}
                onResultSelect={this.handleResultSelect}
                onSearchChange={_.debounce(this.handleSearchChange, 500, {
                    leading: true
                })}
                results={changingList}
                value={value}
                {...this.props}
            />
        )
    }
}
