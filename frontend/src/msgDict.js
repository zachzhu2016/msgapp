import util from './util'

export const CreateGroupSuccess = groupName => {
    return {
        text: `You have successfully created ${groupName} as your new group.`,
        icon: 'success',
        timer: 3000,
        button: false
    }
}

export const JoinGroupSuccess = () => {
    return {
        text: 'you are now part of the group.',
        icon: 'success',
        timer: 3000,
        button: false
    }
}
export const LeaveGroupSuccess = () => {
    return {
        text: 'You have left the group.',
        icon: 'success',
        timer: 3000,
        button: false
    }
}

export const DeleteItemSuccess = item => {
    return {
        text: 'You have permanently deleted an item.',
        icon: 'success',
        timer: 3000,
        button: false
    }
}

export const SignUpSuccess = () => {
    return {
        title: 'Welcome to Groupible!',
        text: 'Now you can proceed to sign in.',
        icon: 'success',
        timer: 5000,
        button: false
    }
}

export const UserUpdateSuccess = () => {
    return {
        text: 'Your informations have been successfully updated.',
        icon: 'success',
        timer: 5000,
        button: false
    }
}

export const PostSuccess = itemName => {
    return {
        text: `You have successfully posted a ${itemName}!`,
        icon: 'success',
        timer: 3000,
        button: false
    }
}

export const ServerError = result => {
    switch (result.status) {
        case 10:
            util.deleteObject(['user', 'token'])
            setTimeout(() => {
                window.location = '/'
            }, 4000)
            return {
                text: 'Your session has expired. Sign in again...',
                timer: 4000,
                button: false,
                icon: 'info'
            } // displays nothing
            break
        case 15:
            return {
                text: 'Sign in to discover more.',
                timer: 4000,
                button: false,
                icon: 'info'
            } // displays nothing
            break
        case 16: // single object not fonud
            return {
                text: 'This object cannot be found.',
                button: false,
                icon: 'error'
            } // displays nothing
            break
        default:
            return {
                text: result
                    ? result.error
                    : "We're encountering a problem. please try again later.",
                icon: 'info',
                button: false
            }
    }
}

export const NetworkError = () => {
    return {
        title: 'Network Error',
        text: 'Check your network and try again.',
        icon: 'error',
        button: false
    }
}

export const NoUserFoundError = () => {
    return {
        title: 'Your information cannot be found.',
        text: 'Please sign in again.',
        icon: 'error',
        button: false
    }
}

export const SessionTimeoutError = () => {
    return {
        title: 'Your session has expired.',
        text: 'Please sign in again.',
        icon: 'warning',
        button: false
    }
}

export const ArgumentError = () => {
    return {
        text: 'You are missing some fields to fill up.',
        icon: 'info',
        button: false,
        timer: 5000
    }
}

export const FetchError = () => {
    return {
        text: 'Failed to fetch information.',
        icon: 'error',
        button: false
    }
}

export const CustomError = text => {
    return {
        text: text ? text : 'Oops... Something went wrong.',
        icon: 'error',
        button: false
    }
}

export const CustomSuccess = text => {
    return {
        text: text ? text : 'Action completed.',
        icon: 'success',
        button: false,
        timer: 3000
    }
}
export const CustomInfo = text => {
    return {
        text: text ? text : 'No info available.',
        icon: 'info',
        button: false
    }
}
