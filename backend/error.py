# error map:
# the mapping order must stay the SAME
errmap = {
        1: {'status': 1}, # success
        2: {'status': 2, 'error': 'User with this email address does not exist.'}, 
        3: {'status': 3, 'error': 'Incorrect password.'}, 
        4: {'status': 4, 'error': 'Argument error.'},
        5: {'status': 5, 'error': "We're encountering a problem. Please try again later."}, # server error
        6: {'status': 6, 'error': 'Failed to update / retrieve this information.'}, # db/orm error, it is not used for now
        7: {'status': 7, 'error': 'This email address is already taken'}, 
        #8: {'status': 8, 'error': 'We currently do not support this organization.'}, 
        8: {'status': 8, 'error': 'Currently, we only authorize people with Lehigh University email addresses to sign in.\n\nMore oragnizations will be added in the future.'}, 
        9: {'status': 9, 'error': 'User must be logged in to see this.'}, 
        10: {'status': 10, 'error': 'Authentication failed.'}, # token error
        11: {'status': 11, 'error': 'Google sign in failed.'}, # google
        12: {'status': 12, 'error': 'You have liked this before.'},  
        13: {'status': 13, 'error': 'You have never liked this before.'},  
        14: {'status': 14, 'error': 'Invalid file type.'}, 
        15: {'status': 15, 'error': 'You must be signed in to take this action.'}, 
        16: {'status': 16, 'error': 'Searched object does not exist.'},
        17: {'status': 17, 'error': 'You have no permission to take this action.'},
        18: {'status': 18, 'error': 'Item does not exist or support this action.'},
        19: {'status': 19, 'error': 'You are not allowed to join this group. Ask for an invitation from the group members.'},
        20: {'status': 20, 'error': 'You are already in this group.'},
        21: {'status': 21, 'error': 'You are not in this group.'},
        22: {'status': 22, 'error': 'Group does not exist.'},
        23: {'status': 23, 'error': 'You are not on the invitee email list.'},
        24: {'status': 24, 'error': 'This link has expired.'},
        25: {'status': 25, 'error': 'Access denied. You do not belong to the organization of this group.'},
        26: {'status': 26, 'error': 'All emails must have the same domain as the organization.'},
        27: {'status': 27, 'error': 'You can only upload at most 25MB of data per feed. Try removing a few files.'},
        28: {'status': 28, 'error': 'Failed to upload.'},
        29: {'status': 29, 'error': 'Failed to send emails. Make sure the email addresses are valid and try again.'},
        }

# (future) return a customized error message with arguments 
def returnerr(errno, *args):
    pass

class AppError(Exception):
    def __init__(self, errdict = None):
        self.errdict = errdict

class FileTypeError(AppError):
    pass

class FileSizeError(AppError):
    pass

class UserPermissionError(AppError):
    pass

class AssociationError(AppError):
    pass

class GroupNotFoundError(AppError):
    pass

class TokenNotFoundError(AppError):
    pass

class GmailError(AppError):
    pass
