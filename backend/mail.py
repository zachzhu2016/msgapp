from __future__ import print_function
from jinja2 import Template
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from datetime import datetime, timedelta
from email.mime.text import MIMEText
import pickle, os.path, base64
from error import GmailError
import os

SCOPES = ["https://www.googleapis.com/auth/gmail.send"]
tempdict = {}
files = [("invite.html", "invite"), 
         ("recovery.html", "recovery")
        ]

for ftuple in files:
    with open(ftuple[0]) as file_:
        tempdict[ftuple[1]] = Template(file_.read())
    dt = datetime.now()
    # shipdate = dt + timedelta(days=(7 - dt.weekday()))
    # tempdict["billing"].globals['shipdate'] = shipdate.strftime("%b %d, %Y")

def create_message(sender, to, subject, message_text):
    """Create a message for an email.

    Args:
    sender: Email address of the sender.
    to: Email address of the receiver.
    subject: The subject of the email message.
    message_text: The text of the email message.

    Returns:
    An object containing a base64url encoded email object.
    """
    message = MIMEText(message_text, "html")
    message['to'] = to
    message['from'] = sender
    message['subject'] = subject
    return {'raw': base64.urlsafe_b64encode(message.as_bytes()).decode()}


def send_message(service, user_id, message):
    """Send an email message.

    Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    message: Message to be sent.

    Returns:
    Sent Message.
    """
    try:
        message = (service.users().messages().send(userId=user_id, body=message).execute())
        print(f"Message Id: {message['id']}")
        return message
    except:
        raise GmailError


def emailmsg(details, template_string):
    """ Generates html for messages with Jinja2 """ 
    return tempdict[template_string].render(**details)

def mailmain():
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            try:
                creds.refresh(Request())
            except:
                os.unlink('token.pickle')
                return
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'gmail_credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('gmail', 'v1', credentials=creds)
    return service

if __name__ == "__main__":
    mailmain() 
