
echo "\n -> Start logging in AWS ECR...\n"

$(aws ecr get-login --no-include-email --region us-east-1)

echo "\n -> Building new image...\n"

docker build -t 540088482516.dkr.ecr.us-east-1.amazonaws.com/groupible:latest -f Dockerfile .

echo "\n -> Pushing new image to AWS ECR\n"

docker push 540088482516.dkr.ecr.us-east-1.amazonaws.com/groupible:latest

echo "\n -> Forcing new deployment...\n"

aws ecs update-service --cluster msgapp-cluster --service msgapp-core-services --force-new-deployment

echo "\n -> Deployment finished.\n"
