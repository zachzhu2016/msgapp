from pprint import pprint 
import json
import requests

# utility functions

def urlfor(fname, someid = None):
    if fname == 'register':
        url = f'{domain}/register'
    elif fname == 'login':
        url = f'{domain}/login'
    elif fname == 'feed':
        if someid:
            url = f'{domain}/feed/{someid}'
        else:
            url = f'{domain}/feed'
    elif fname == 'new_group':
        url = f'{domain}/group/new'
    elif fname == 'new_feed':
        url = f'{domain}/group/{someid}/feed/new'
    elif fname == 'new_comment':
        url = f'{domain}/feed/{someid}/comment/new'
    elif fname == 'myfeed':
        url = f'{domain}/feed?view=mine'
    elif fname == 'mygroup':
        url = f'{domain}/group?view=mine'
    elif fname == 'userfeed':
        url = f'{domain}/user/{someid}/feed'
    elif fname == 'groupfeed':
        url = f'{domain}/group/{someid}/feed'
    elif fname == 'groupuser':
        url = f'{domain}/group/{someid}/user'
    elif fname == 'usergroup':
        url = f'{domain}/user/{someid}/group'
    elif fname == 'join_group':
        url = f'{domain}/group/{someid}/join'
    elif fname == 'leave_group':
        url = f'{domain}/group/{someid}/leave'
    
    return url if url else ''

def sender(url, headers = dict(), json_dict = dict(), data_dict = dict(), method = 'GET'):
    if method == 'GET':
        response1 = requests.get(url = url, headers = {'Authorization': tmap.get('zi')})
        response2 = requests.get(url = url, headers = {'Authorization': tmap.get('daniel')})
        response3 = requests.get(url = url, headers = {'Authorization': tmap.get('wang')})
    elif method == 'POST':
        response1 = requests.post(url = url, headers = {'Authorization': tmap.get('zi'), **headers}, json = json_dict.get('zi'), data = data_dict.get('zi'))
        response2 = requests.post(url = url, headers = {'Authorization': tmap.get('daniel'), **headers}, json = json_dict.get('daniel'), data = data_dict.get('daniel'))
        response3 = requests.post(url = url, headers = {'Authorization': tmap.get('wang'), **headers}, json = json_dict.get('wang'), data = data_dict.get('wang'))
    elif method == 'PUT':
        response1 = requests.put(url = url, headers = {'Authorization': tmap.get('zi'), **headers}, json = json_dict.get('zi'), data = data_dict.get('zi'))
        response2 = requests.put(url = url, headers = {'Authorization': tmap.get('daniel'), **headers}, json = json_dict.get('daniel'), data = data_dict.get('daniel'))
        response3 = requests.put(url = url, headers = {'Authorization': tmap.get('wang'), **headers}, json = json_dict.get('wang'), data = data_dict.get('wang'))
    elif method == 'DELETE':
        response1 = requests.delete(url = url, headers = {'Authorization': tmap.get('zi')})
        response2 = requests.delete(url = url, headers = {'Authorization': tmap.get('daniel')})
        response3 = requests.delete(url = url, headers = {'Authorization': tmap.get('wang')})

    if response1 and response2 and response3:
        print('zi:')
        pprint(response1.json())
        print('daniel:')
        pprint(response2.json())
        print('wang:')
        pprint(response3.json())
    else:
        print('method not allowed')

# testers
def test_userfeed(user_id):
    sender(urlfor('userfeed', user_id), method = 'GET')

def test_groupfeed(group_id): # non-function
    sender(urlfor('groupfeed', group_id), method = 'GET')

def test_groupuser(group_id): # non-function
    sender(urlfor('groupuser', group_id), method = 'GET')

def test_usergroup(user_id):
    sender(urlfor('usergroup', user_id), method = 'GET')

def test_myfeed():
    sender(urlfor('myfeed'), method = 'GET')

def test_mygroup():
    sender(urlfor('mygroup'), method = 'GET')

def test_feed():
    response = requests.get(url = urlfor('feed'))
    pprint(response.json())

def test_onefeed(feed_id):
    sender(urlfor('feed', feed_id), method = 'GET')

def test_new_comment(parent_id):
    bodies = {
            'zi': {
                'content': 'zi comment content',
            },
            'daniel': {
                'content': 'daniel comment content',
            },
            'wang': {
                'content': 'wang comment content',
            }
    }
    sender(url = urlfor('new_comment', parent_id), json_dict = bodies, method = 'POST')

def test_new_feed(group_id):
    bodies = {
            'zi': {
                'title': 'zi feed',
                'body': 'zi content',
            },
            'daniel': {
                'title': 'daniel feed',
                'body': 'daniel content',
            },
            'wang': {
                'title': 'wang feed',
                'body': 'wang content',
            }
    }
    sender(url = urlfor('new_feed', group_id), data_dict = bodies, method = 'POST')

def test_new_group():
    # public
    bodies = {
            'zi': {'name': 'zi public group', 'about': 'about zi', 'is_public': 1},
            'daniel': {'name': 'daniel public group', 'about': 'about daniel', 'is_public': 1},
            'wang': {'name': 'wang public group', 'about': 'about wang', 'is_public': 1}
    }

    sender(url = urlfor('new_group'), headers = {'content-type': 'application/json'}, json_dict = bodies, method = 'POST')
    # private
    bodies = {
            'zi': {'name': 'zi private group', 'about': 'about zi', 'is_public': 0},
            'daniel': {'name': 'daniel private group', 'about': 'about daniel', 'is_public': 0},
            'wang': {'name': 'wang private group', 'about': 'about wang', 'is_public': 0}
    }
    sender(url = urlfor('new_group'), headers = {'content-type': 'application/json'}, json_dict = bodies, method = 'POST')

def test_register():
    bodies = {
            'zi': {
                'firstname': 'Ziyuan',
                'lastname': 'Zhu',
                'email': 'zi@gmail.com',
                'password': 'secret'
            },
            'daniel': {
                'firstname': 'Daniel',
                'lastname': 'Kim',
                'email': 'daniel@gmail.com',
                'password': 'secret'
            },
            'wang': {
                'firstname': 'Ruifan',
                'lastname': 'Wang',
                'email': 'wang@gmail.com',
                'password': 'secret'
            }
    }
    sender(url = urlfor('register'), headers = {'content-type': 'application/json'}, json_dict = bodies, method = 'POST')

def test_login():
    bodies = {
            'zi': {
                'email': 'zi@gmail.com',
                'password': 'secret'
            },
            'daniel': {
                'email': 'daniel@gmail.com',
                'password': 'secret'
            },
            'wang': {
                'email': 'wang@gmail.com',
                'password': 'secret'
            }
    }
    
    response1 = requests.post(url = urlfor('login'), headers = {'content-type': 'application/json'}, json = {'email': 'zi@gmail.com','password': 'secret'}) 
    response2 = requests.post(url = urlfor('login'), headers = {'content-type': 'application/json'}, json = {'email': 'daniel@gmail.com','password': 'secret'}) 
    response3 = requests.post(url = urlfor('login'), headers = {'content-type': 'application/json'}, json = {'email': 'wang@gmail.com','password': 'secret'}) 

    tmap['zi'] = response1.json()['token']
    tmap['daniel'] = response2.json()['token']
    tmap['wang'] = response3.json()['token']

    pprint(response1.json()['status'])
    pprint(response2.json()['status'])
    pprint(response3.json()['status'])

def test_join_group(group_id):
    sender(url = urlfor('join_group', group_id), method = 'POST')

def test_leave_group(group_id):
    sender(url = urlfor('leave_group', group_id), method = 'DELETE')
"""

Requirements:
    1. Database must initialy be empty
    2. functions must be run in order

TEST 1:
    test_register 
    test_login 
    test_new_group 
    test_new_feed  
    test_get_feed  
    test_get_onefeed

"""

if __name__ == '__main__':

    domain = 'http://localhost:80'
    # domain = 'https://api.groupible.com'

    # stored token
    tmap = {'nobody': '',
            'zi': '',
            'daniel': '',
            'wang': ''}


