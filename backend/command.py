import subprocess
import os
import sys

proddb = 'msgapp-database.cdgwlext2hyw.us-east-1.rds.amazonaws.com'
devdb = '192.168.99.100'

# env
os.environ['POSTGRES_IP'] = "192.168.99.100" 
os.environ['POSTGRES_PORT'] = "5432" 
os.environ['POSTGRES_USER'] = "ziz221" 
os.environ['POSTGRES_PASS'] = "123456" 

# command map
cmap = {
    1: ['python3', 'orm.py', 'ziz221', '123456', devdb, '5432', 'postgres'],
    2: ['python3', 'orm.py', 'ziz221', 'helloworld', proddb, '5432', 'postgres'],
    3: ["docker", "exec", "-it", "testdb", "psql", "postgres", "-h", "127.0.0.1", "-U", "ziz221", "-p", "5432"],
    4: ["psql", "postgres", "-h", proddb, "-p", "5432", "-U", "ziz221"],
    5: ["docker", "run", "--name", "testdb", "--env", "POSTGRES_USER", "--env", "POSTGRES_PASSWORD", "-p", "5432:5432", "-d", "postgres"],
    6: ["python3", "app.py", "ziz221", "123456", devdb, "5432", "postgres"],
    7: ["python3", "scraper.py", "ziz221", "123456", devdb, "5432", "postgres"],
    8: ["python3", "scraper.py", "ziz221", "helloworld", proddb, "5432", "postgres"]
    }


print("1. Configure development Docker database.")
print("2. Configure production RDS database.")
print("3. Connect to development database interface.")
print("4. Connect to production database interface.")
print("5. Create Docker Postgres database instance on local.")
print("6. Serve local development app.")
print("7. Run scraper (push to development database).")
print("8. Run scraper (push to production database).")

if len(sys.argv) > 1:
    command = int(sys.argv[1])
else:
    command = int(input()) 

subprocess.call(cmap[command])
