/* adds delete cascade
alter table public.items
drop constraint items_parent_id_fkey,
add constraint items_parent_id_fkey
   foreign key (parent_id)
   references items(id)
   on delete cascade;
*/
