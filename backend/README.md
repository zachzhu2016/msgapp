﻿# Welcome to Groupible API!

(Introduction)

# ROUTES

## Get public feeds
 - URL

> /feed

 - Method: 
 GET

 - Success Response:
> { 'status': 1, 'feeds': [{...}, {...}] }

 - Error Response:
> {'status': 15, 'error': 'No objects found'}
> {'status': 5, 'error': 'We're encountering a problem. It's not yours.'}

## Get comments on a feed
 - URL

> /feed/:feed_id/comment

 - Method: 
 GET

- Optional Headers:
'Authorization': (your token string)

 - Success Response:
> { 'status': 1, 'comments': [{...}, {...}] }

 - Error Response:
> {'status': 5, 'error': 'We're encountering a problem. It's not yours.'}
> {'status': 10, 'error': 'Authentication failed.'}
> {'status': 15, 'error': 'No objects found'}
> {'status': 16, 'error': 'Object does not exist.'}
> {'status': 18, 'error': 'You have no permission to view this.'}

## Get users
 - URL

> /user

 - Method: 
 GET

 - Required JSON Body Params:
 None
 
 - Success Response:
> { 'status': 1, 'users': [{...}, {...}] }

 - Error Response:
> {'status': 15, 'error': 'No objects found'}
> {'status': 5, 'error': 'We're encountering a problem. It's not yours.'}

## Get public groups 
- URL

> /group

 - Method: 
 GET

 - Success Response:
> { 'status': 1, 'groups': [{...}, {...}] }

 - Error Response:
> {'status': 15, 'error': 'No objects found'}
> {'status': 5, 'error': 'We're encountering a problem. It's not yours.'}

## Get a feed 
- URL

> /feed/:feed_id

 - Method: 
 GET

- Optional Headers:
'Authorization': (your token string)

 - Success Response:
> { 'status': 1, 'feed': {..}}

 - Error Response:
> {'status': 5, 'error': 'We're encountering a problem. It's not yours.'}
> {'status': 10, 'error': 'Authentication failed.'}
> {'status': 16, 'error': 'Object does not exist.'}
> {'status': 18, 'error': 'You have no permission to view this.'}

## Get a user 
- URL

> /user/:user_id

 - Method: 
 GET

 - Success Response:
> { 'status': 1, 'feed': {..}}

 - Error Response:
> {'status': 5, 'error': 'We're encountering a problem. It's not yours.'}
> ~~{'status': 10, 'error': 'Authentication failed.'}~~
> {'status': 16, 'error': 'Object does not exist.'}
> ~~{'status': 18, 'error': 'You have no permission to view this.'}~~

## Get one group 
- URL

> /group/:group_id

 - Method: 
 GET

- Optional Headers:
'Authorization': (your token string)

 - Success Response:
> { 'status': 1, 'feed': {..}}

 - Error Response:
> {'status': 5, 'error': 'We're encountering a problem. It's not yours.'}
> {'status': 10, 'error': 'Authentication failed.'}
> {'status': 16, 'error': 'Object does not exist.'}
> {'status': 18, 'error': 'You have no permission to view this.'}

## Get public feeds posted by a user 
- URL

> /user/:user_id/feed

 - Method: 
 GET

 - Success Response:
> { 'status': 1, 'feeds': {..}}

 - Error Response:
> {'status': 15, 'error': 'No objects found'}
> {'status': 5, 'error': 'We're encountering a problem. It's not yours.'}

## Get public groups that a user is in
- URL

> /user/:user_id/group

 - Method: 
 GET

 - Success Response:
> { 'status': 1, 'groups': {..}}

 - Error Response:
> {'status': 15, 'error': 'No objects found'}
> {'status': 5, 'error': 'We're encountering a problem. It's not yours.'}

## Get ALL feeds posted by current user
- URL

> /feed/mine

 - Method: 
 GET

- Required Headers:
'Authorization': (your token string)

 - Success Response:
> { 'status': 1, 'feeds': {..}}

 - Error Response:
> {'status': 5, 'error': 'We're encountering a problem. It's not yours.'}
> {'status': 10, 'error': 'Authentication failed.'}
> {'status': 15, 'error': 'No objects found'}

## Get ALL groups that current user is in
- URL

> /group/mine

 - Method: 
 GET

- Required Headers:
'Authorization': (your token string)

 - Success Response:
> { 'status': 1, 'feeds': {..}}

 - Error Response:
> {'status': 5, 'error': 'We're encountering a problem. It's not yours.'}
> {'status': 10, 'error': 'Authentication failed.'}
> {'status': 15, 'error': 'No objects found'}

## Get all feeds posted in a group 
- URL

> /group/:group_id/feed

 - Method: 
 GET

- Optional Headers:
'Authorization': (your token string)

 - Success Response:
> { 'status': 1, 'feed': {..}}

 - Error Response:
> {'status': 5, 'error': 'We're encountering a problem. It's not yours.'}
> {'status': 10, 'error': 'Authentication failed.'}
> {'status': 16, 'error': 'Object does not exist.'}
> {'status': 18, 'error': 'You have no permission to view this.'}

## Get all users in a group 
- URL

> /group/:group_id/user

 - Method: 
 GET

- Optional Headers:
'Authorization': (your token string)

 - Success Response:
> { 'status': 1, 'feed': {..}}

 - Error Response:
> {'status': 5, 'error': 'We're encountering a problem. It's not yours.'}
> {'status': 10, 'error': 'Authentication failed.'}
> {'status': 16, 'error': 'Object does not exist.'}
> {'status': 18, 'error': 'You have no permission to view this.'}
