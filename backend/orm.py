from sqlalchemy import create_engine, Column, String, Integer, ARRAY, DateTime, func, Boolean, ForeignKey, Table, create_engine
from sqlalchemy.orm import backref, relationship, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import datetime
import psycopg2
import sys
import os
func.now()
Base = declarative_base()

def to_dict(obj: Base, exclude = set()):
    
    """ Returns dictionary from SQLAlchemy Object """ 

    exclude = exclude.union({"password", "status"})

    return {c.name: (getattr(obj, c.name, '') if not isinstance(getattr(obj, c.name, None), datetime.datetime) else getattr(obj, c.name, None).isoformat()) for c in obj.__table__.columns if not (exclude and c.name in exclude)}

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    type = Column(String, default='user', nullable=False) # admin, google
    firstname = Column(String)
    lastname = Column(String)
    email = Column(String)
    phone_number = Column(String)
    password = Column(String) # password hash
    about = Column(String)
    profpic_key = Column(String)
    status = Column(Integer, default=1) # 1 = active; 0 = banned
    created = Column(DateTime(timezone=True), default=func.now())
    # fk
    org_id = Column(Integer, ForeignKey('organizations.id'), nullable=False)
    # relationships
    like_relations = relationship("LikeRelation", back_populates="user")
    fav_relations = relationship("FavoriteRelation", back_populates="user")
    join_relations = relationship("JoinRelation", back_populates="user")
    organization = relationship("Organization", backref=backref("users", lazy='dynamic'))

class Organization(Base):
    __tablename__ = 'organizations'
    id = Column(Integer, primary_key=True)
    type = Column(String)
    name = Column(String, nullable=False)
    about = Column(String)
    city = Column(String)
    state = Column(String)
    domain = Column(String, nullable=False) # ziz221@lehigh.edu => "lehigh.edu"
    profpic_key = Column(String)
    status = Column(Integer, default=1) # 1 = active; 0 = banned
    created = Column(DateTime(timezone=True), default=func.now())


class Group(Base):
    __tablename__ = 'groups'
    id = Column(Integer, primary_key=True)
    type = Column(String) # { categories }
    name = Column(String, nullable=False)
    about = Column(String)
    profpic_key = Column(String)
    is_public = Column(Integer, nullable=False) # 1 = public, 0 = no
    status = Column(Integer, default=1) # 1 = active; 0 = banned
    created = Column(DateTime(timezone=True), default=func.now())
    # fk
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False) # creator
    org_id = Column(Integer, ForeignKey('organizations.id'), nullable=False) 
    # relationship
    join_relations = relationship("JoinRelation", back_populates="group")
    fav_relations = relationship("FavoriteRelation", back_populates="group")
    organization = relationship("Organization", backref=backref("groups", lazy='dynamic'))
    creator = relationship("User", backref=backref("groups", lazy='dynamic'))
    
class Item(Base):
    __tablename__ = 'items'
    id = Column(Integer, primary_key=True)
    type = Column(String) 
    like_count = Column(Integer, default=0)
    status = Column(Integer, default=1) # 1 = active; 0 = inactive
    created = Column(DateTime(timezone=True), default=func.now())
    modified = Column(DateTime(timezone=True), onupdate=func.now())
    # fk
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False) # author
    group_id = Column(Integer, ForeignKey('groups.id'), nullable=False) 
    org_id = Column(Integer, ForeignKey('organizations.id'), nullable=False) 
    # relationship
    like_relations = relationship("LikeRelation", back_populates="item", cascade="delete")
    organization = relationship("Organization", backref=backref("items", lazy='dynamic'))
    author = relationship("User", backref=backref("items", lazy='dynamic'))
    group = relationship("Group", backref=backref("items", lazy='dynamic'))

    __mapper_args__ = {
            'polymorphic_on':type,
            'polymorphic_identity':'item'
            }

class Feed(Item):
    tag = Column(String)
    title = Column(String)
    body = Column(String)
    comment_count = Column(Integer, default=0)
    file_keys = Column(ARRAY(String))
    comments = relationship('Comment', lazy='dynamic', order_by="Comment.id", cascade="delete") # one parent to many children relationship
    __mapper_args__ = {
            'polymorphic_identity':'feed'
            }

class Comment(Item):
    content = Column(String)
    parent_id = Column(Integer, ForeignKey('items.id'))
    __mapper_args__ = {
            'polymorphic_identity':'comment'
            }

class InviteCode(Base):
    __tablename__ = 'invite_codes'
    id = Column(String(32), primary_key=True)
    invitor_id = Column(Integer, ForeignKey('users.id'))
    group_id = Column(Integer, ForeignKey('groups.id'))
    scope = Column(String)
    invitee_emails = Column(ARRAY(String))
    created = Column(DateTime(timezone=True), default=func.now())

# user - item
class LikeRelation(Base):
    __tablename__ = 'like_relations'
    user_id = Column(Integer, ForeignKey('users.id'), primary_key=True)
    item_id = Column(Integer, ForeignKey('items.id'), primary_key=True)
    user = relationship("User", back_populates="like_relations")
    item = relationship("Item", back_populates="like_relations")

# user - group
class JoinRelation(Base):
    __tablename__ = 'join_relations'
    user_id = Column(Integer, ForeignKey('users.id'), primary_key=True)
    group_id = Column(Integer, ForeignKey('groups.id'), primary_key=True)
    created = Column(DateTime(timezone=True), default=func.now())
    
    user = relationship("User", back_populates="join_relations")
    group = relationship("Group", back_populates="join_relations")

# user - group
class FavoriteRelation(Base):
    __tablename__ = 'fav_relations'
    user_id = Column(Integer, ForeignKey('users.id'), primary_key=True)
    group_id = Column(Integer, ForeignKey('groups.id'), primary_key=True)
    user = relationship("User", back_populates="fav_relations")
    group = relationship("Group", back_populates="fav_relations")


if __name__ == '__main__':
    #try:
    #    username,password,host,port,database = sys.argv[1:6]
    #except:
    #    raise Exception
    username,password,host,port,database = "ziz221", "123456", "192.168.99.100", "5432", "postgres"
    engine = create_engine(f"postgresql+psycopg2://{username}:{password}@{host}:{port}/{database}")
    session = sessionmaker(bind=engine)
    #Base.metadata.create_all(engine)
    
    # populate existing organizations
    """
    org_tuples = [
            ("Lehigh University", "A private university", "Bethlehem", "PA", "lehigh.edu", "organizations/1/profile/profpic.jpg")
            ]
    user_tuples = [("bot", "Lehigh University", "2020 Spring Semester", "Official account that syncs with Lehigh Courses", 1)]
    sess = session() 
    try: 
        for t in org_tuples:
            org = Organization(name=t[0], about=t[1], city=t[2], state=t[3], domain=t[4], profpic_key=t[5], status=1)
            sess.add(org)
        sess.commit()
        for t in user_tuples:
            bot = User(type=t[0], firstname=t[1], lastname=t[2], about=t[3], org_id=t[4])
            sess.add(bot)
        sess.commit()
    finally:
        sess.close()
    """

