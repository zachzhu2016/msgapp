from selectolax.parser import HTMLParser
import urllib.request
from orm import User, Group, JoinRelation, to_dict
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import sys, html, json, requests, unicodedata
import html.parser as htmlparser
import boto3

from pprint import pprint

coursesite_headers = {
    "Host": "coursesite.lehigh.edu",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Accept-Language": "en-US,en;q=0.5",
    "Accept-Encoding": "gzip, deflate, br",
    "Referer": "https://coursesite.lehigh.edu/auth/saml/login.php?errorcode=4",
    "Connection": "keep-alive",
    "Cookie": "_ga=GA1.2.2030273141.1537154339; _ga=GA1.3.2030273141.1537154339; __qca=P0-767675706-1552669784996; _fbp=fb.1.1570072839321.1487594622; __utmc=182352186; _pk_testcookie.5.be30=1; __utma=182352186.2030273141.1537154339.1572928581.1579525593.17; __utmz=182352186.1579525593.17.17.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); IDMSESSID=83F85113F210FE9A06E529E47ABA9BE80BD9442D4254ACCC1988A4FD530E2017B3B9B7B3CF222ED646352565BD204C6B3F9B858D0A94D0BF8258ACDDBFA3FD4E; _gid=GA1.2.439984560.1580064171; _gid=GA1.3.439984560.1580064171; _gat_UA-3168927-1=1; _gat_UA-9082157-6=1; SimpleSAMLSessionID=6cd3c5510d3d81a7aabeed0b51d8a286; SimpleSAMLAuthToken=_9dc4e7925c0bc03e536ea2d86167db4121bb1dfa44; MoodleSession=oo9hdbcrek2a2i04273i4pldj6; MOODLEID1_=%253FO%25AA%25E3%2599%25C7; _pk_ses.5.be30=1; _pk_id.5.be30=6be98506d23dc772.1579151485.27.1580152217.1580152213.",
    "Upgrade-Insecure-Requests": "1"
}

course_desc_headers = {
    "Host": "registration.lehigh.edu",
    "Connection": "keep-alive",
    "Content-Length": "39",
    "Accept": "text/html, */*; q=0.01",
    "X-Synchronizer-Token": "46807c3b-1a13-4cd8-a81d-576045c0f326",
    "Origin": "https://registration.lehigh.edu",
    "X-Requested-With": "XMLHttpRequest",
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
    "Sec-Fetch-Site": "same-origin",
    "Sec-Fetch-Mode": "cors",
    "Referer": "https://registration.lehigh.edu/StudentRegistrationSsb/ssb/classSearch/classSearch",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "en-US,en;q=0.9,ko;q=0.8,zh-CN;q=0.7,zh;q=0.6",
    "Cookie": "JSESSIONID=4E2536854E220736E55BFAED274C23DD; _ga=GA1.2.2030273141.1537154339; __qca=P0-767675706-1552669784996; _fbp=fb.1.1570072839321.1487594622; __utma=182352186.2030273141.1537154339.1572155343.1572928581.16; __utmc=182352186; __utmz=182352186.1572928581.16.16.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); SERVERID=thoth3; _gid=GA1.2.1246762699.1579079873; IDMSESSID=7838535A0300B66181E71D91317A2188E0CD06BD4833EE1280AEA265601695B2387942B30186AE548E0BAFA1893D363602C9A3F22D8B67D79FB583DE5E75709A; JSESSIONID=E764271BA5F503F984E2A7C4FC8A91DC"
}

courses_headers = {
        "Host": "registration.lehigh.edu",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:72.0) Gecko/20100101 Firefox/72.0",
        "Accept": "application/json, text/javascript, */*; q=0.01",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "X-Synchronizer-Token": "2c41cf6e-924b-4a5d-a97b-5cc4fd6f19c4",
        "X-Requested-With": "XMLHttpRequest",
        "DNT": "1",
        "Connection": "keep-alive",
        "Referer": "https://registration.lehigh.edu/StudentRegistrationSsb/ssb/classSearch/classSearch",
        "Cookie": "JSESSIONID=75838437E4FE9ACD46456E240F985445; _ga=GA1.2.676781517.1486611227; __utma=182352186.676781517.1486611227.1564817287.1564817287.1; __utmz=182352186.1564817287.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); SERVERID=thoth3; _gid=GA1.2.2001976102.1579729486; _gat_Ellucian=1",
        "Pragma": "no-cache",
        "Cache-Control": "no-cache"
}



opener = urllib.request.build_opener()
opener.addheaders = [(k, v) for k, v in coursesite_headers.items()]
urllib.request.install_opener(opener)

def get_user_urls():
    url = "https://coursesite.lehigh.edu/user/index.php?contextid=1396877&id=70682&perpage=6839"
    count = 0
    response = requests.get(url = url, headers = coursesite_headers)
    html = response.text
    selector = 'a[href*="user/view.php"]'
    with open("user_url_list.txt", "w") as fp:
        for node in HTMLParser(html).css(selector):
            fp.write(f'{node.attrs["href"]}\n')
            count += 1
    print(count)


def get_user(url):

    user = {'firstname': '', 'lastname': '', 'email': '', 'about': ''}

    response = requests.get(url = url, headers = coursesite_headers)
    page = response.text
    
    tree = HTMLParser(page).css_first('.userprofile')
    tree.unwrap_tags(['b'])
    for node in tree.traverse():
        if node.tag == 'img': # profile pic
            urllib.request.urlretrieve(node.attrs["src"], 'image.png')

        if node.tag == 'h2': # full name
            user['firstname'], user['lastname'] = unicodedata.normalize("NFKD", node.text().split(' ')[0]), unicodedata.normalize("NFKD", node.text().split(' ')[1])
        if node.attrs.get('class') == 'no-overflow': # about
            user['about'] += f'{unicodedata.normalize("NFKD", node.text(deep=False))}\n'
        if node.tag == 'p': # about
            user['about'] += f'{unicodedata.normalize("NFKD", node.text())}\n\n'
        if node.tag == 'a' and 'mailto' in node.attrs.get('href'): # email address
            email = node.text() 
            if 'lehigh' not in email:
                return None
            user['email'] = unicodedata.normalize("NFKD", email)

    return user if (user.get('email') and user.get('firstname') and user.get('lastname')) else None

def upload_users():
    sess = Session()
    try:
        with open("user_url_list.txt", "r") as fp:
            lines = fp.readlines()
            urls = [line.strip() for line in lines]
            #for i in range(len(urls)): 
            for i in range(5500, 6500): 
                try:
                    user = get_user(urls[i])
                    if user:
                        pprint(user) # indicator
                        newuser = User(firstname=user['firstname'], lastname=user['lastname'], email=user['email'], about=user['about'], org_id=1)
                        sess.add(newuser)
                        sess.flush()

                        key = f'users/{newuser.id}/profile/profpic.png'
                        with open("image.png", "rb") as fp:
                            S3.put_object(Bucket=BUCKET, Key=key, Body=fp)
                        newuser.profpic_key = key
                        pprint(key)
                except Exception as e:
                    print(e)
                    pass
            sess.commit()
    except Exception as e:
        sess.rollback()
        print(e)
    finally:
        sess.close()

def download_class_objects():
    classes = []
    size = 3901
    offset = 500
    current = 0
    while current < size:
        url = f'https://registration.lehigh.edu/StudentRegistrationSsb/ssb/searchResults/searchResults?txt_term=202010&pageOffset={current}&pageMaxSize={current + offset}&sortColumn=subjectDescription&sortDirection=asc'
        response = requests.post(url = url, headers = courses_headers)
        data = json.loads(response.text)['data']
        classes += data
        current += offset
    print(len(classes))
    with open('class_object_list.json', 'a', encoding='utf-8') as fp:
        json.dump(classes, fp, ensure_ascii=False, indent=4)

def get_class_description(crn):
    url = "https://registration.lehigh.edu/StudentRegistrationSsb/ssb/searchResults/getCourseDescription"
    payload = {"term": "202010", "courseReferenceNumber": crn}
    response = requests.post(url = url, data = payload, headers = course_desc_headers)
    return parser.unescape(response.text)

def upload_classes():
    sess = Session()
    try:
        added = set()
        with open('class_object_list.json', 'r') as fp:
            classes = json.load(fp)
            #for i in range(50):
            for c in classes: 
                #c = classes[i]
                if c['subjectCourse'] not in added:
                    desc = get_class_description(c['courseReferenceNumber'])
                    print(c['subjectCourse'])
                    added.add(c['subjectCourse'])
                    name = f'{c["courseTitle"]} ({c["subjectCourse"]})'
                    newgroup = Group(name=name, about=desc, is_public=1, org_id=1, user_id=1)
                    sess.add(newgroup)
                    sess.flush()
                    newjoin = JoinRelation(user_id=1, group_id=newgroup.id)
                    sess.add(newjoin)
        sess.commit()
    except Exception as e:
        sess.rollback()
        print(e)
    finally:
        sess.close()

if __name__ == '__main__':
    try:
        username,password,host,port,database = sys.argv[1:]
    except:
        raise Exception
    engine = create_engine(f"postgresql+psycopg2://{username}:{password}@{host}:{port}/{database}", pool_pre_ping=True)
    Session = sessionmaker(bind=engine)
    S3 = boto3.client('s3')
    BUCKET = 'groupible'

    parser = htmlparser.HTMLParser()

    #url = "https://coursesite.lehigh.edu/user/view.php?id=51757&course=70682"
    upload_users()
    #upload_classes()
    #download_class_objects()
