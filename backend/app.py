# general
import sys, os, json, sqlite3, time, base64, itertools, ssl
from datetime import datetime, timezone
from passlib.hash import argon2
from pprint import pprint
from contextlib import contextmanager
from random import randint
from hashlib import md5
# jwt
import jwt
from jwt.exceptions import InvalidTokenError
# my files
from error import *
from mail import *
# flask
from flask import Flask, request, render_template, send_from_directory, jsonify
from werkzeug.utils import secure_filename 
from flask_cors import CORS, cross_origin 
# orm
from orm import Base, User, Item, Feed, Comment, Group, Organization, LikeRelation, FavoriteRelation, JoinRelation, InviteCode, to_dict
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
# google sign-in
from google.oauth2 import id_token
from google.auth.transport import requests
# aws S3 
import boto3
from botocore.exceptions import ClientError

# app config
ROOT = 'groupible.com'
BASE_DIR = os.path.dirname(__file__)
app = Flask(__name__)
CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['MAX_CONTENT_LENGTH'] = 20 * 1024 * 1024

# debugging only
os.environ["FLASK_ENV"] = "development"

@app.route('/healthcheck', methods=['GET'])
def healthcheck():
    # add a select statement to check if sqlalchemy is working correctly (future)
    return {"status": 1}


# getters
@app.route('/feed', methods=['GET'])
def get_feeds():
    args = request.args
    view = args.get("view") 
    tstring = request.headers.get('Authorization')
    try:
        with session_scope() as sess:
            if view and view == "mine":
                token = resolve_token(tstring)
                query = sess.query(Feed).filter_by(user_id=token['user_id'])
            else:
                gids = sess.query(Group.id).filter_by(is_public=1).all()
                query = sess.query(Feed).filter(Feed.group_id.in_(gids))
            
            feeds = fetchobjs(query, args.get('page'), args.get('page_size'))
            return returnobjs(feeds, 'feeds', query.count(), fks = ['author'])
    except AppError as e:
        return e.errdict

@app.route('/group/<int:gid>/feed', methods=['GET'])
@app.route('/group/<int:gid>/file', methods=['GET'])
def get_groupfeeds(gid):
    args = request.args
    tstring = request.headers.get('Authorization')
    try:
        with session_scope() as sess:
            rdict = resolve_permission(sess, gid, tstring, allow_public = True)
            query = rdict['group'].items.filter_by(type='feed')
            feeds = fetchobjs(query, args.get('page'), args.get('page_size'))
            if 'file' in request.path:
                return returnobjs(feeds, 'files', query.count(), fks = ['author'], get_files = True) 
            return returnobjs(feeds, 'feeds', query.count(), fks = ['author'])
    except AppError as e:
        return e.errdict

@app.route('/user/<int:uid>/feed', methods=['GET'])
def get_userfeeds(uid):
    args = request.args
    tstring = request.headers.get('Authorization')
    try:
        with session_scope() as sess:
            query = sess.query(Feed).filter_by(user_id=uid).filter(Feed.group.has(is_public=1))
            feeds = fetchobjs(query, args.get('page'), args.get('page_size'))
            return returnobjs(feeds, 'feeds', query.count(), fks = ['author'])
    except AppError as e:
        return e.errdict

@app.route('/user', methods=['GET'])
def get_users():
    """ Get all public users with no token """
    args = request.args
    try: 
        with session_scope() as sess:
            query = sess.query(User)
            users = fetchobjs(query, args.get('page'), args.get('page_size'))
            return returnobjs(users, 'users', query.count())
    except AppError as e:
        return e.errdict

@app.route('/group/<int:gid>/user', methods=['GET'])
#TODO
def get_groupusers(gid):
    args = request.args
    tstring = request.headers.get('Authorization')
    try: 
        with session_scope() as sess:
            rdict = resolve_permission(sess, gid, tstring, allow_public = True)
            query = sess.query(User).filter(User.join_relations.any(group_id=gid))
            users = fetchobjs(query, args.get('page'), args.get('page_size'))
            return returnobjs(users, 'users', query.count())
    except AppError as e:
        return e.errdict

@app.route('/organization', methods=['GET'])
def get_organizations():
    args = request.args
    try:
        with session_scope() as sess:
            query = sess.query(Organization)
            orgs = fetchobjs(query, args.get('page'), args.get('page_size'))
            return returnobjs(orgs, 'organizations', query.count(), fks = [])
    except AppError as e:
        return e.errdict

@app.route('/organization/<int:oid>/user', methods=['GET'])
def get_orgusers(oid):
    args = request.args
    try: 
        with session_scope() as sess:
            query = sess.query(User).filter_by(org_id=oid)
            orgs = fetchobjs(query, args.get('page'), args.get('page_size'))
            return returnobjs(orgs, 'users', query.count())
    except AppError as e:
        return e.errdict

@app.route('/organization/<int:oid>/feed', methods=['GET'])
def get_orgfeeds(oid):
    args = request.args
    try: 
        with session_scope() as sess:
            gids = sess.query(Group.id).filter_by(org_id=oid, is_public=1)
            query = sess.query(Feed).filter(Feed.group_id.in_(gids))
            feeds = fetchobjs(query, args.get('page'), args.get('page_size'))
            return returnobjs(feeds, 'feeds', query.count())
    except AppError as e:
        return e.errdict

@app.route('/organization/<int:oid>/group', methods=['GET'])
def get_orggroups(oid):
    args = request.args
    try: 
        with session_scope() as sess:
            query = sess.query(Group).filter_by(org_id=oid, is_public=1)
            groups = fetchobjs(query, args.get('page'), args.get('page_size'))
            return returnobjs(groups, 'groups', query.count())
    except AppError as e:
        return e.errdict

@app.route('/group', methods=['GET'])
def get_groups(uid = None):
    args = request.args
    view = request.args.get("view") 
    tstring = request.headers.get('Authorization')
    try: 
        with session_scope() as sess:
            if view and view == "mine": 
                token = resolve_token(tstring)
                query = sess.query(Group).filter(Group.join_relations.any(user_id=token['user_id']))
            else:
                query = sess.query(Group).filter_by(is_public=1)
            groups = fetchobjs(query, args.get('page'), args.get('page_size'))
            return returnobjs(groups, 'groups', query.count())
    except AppError as e:
        return e.errdict

@app.route('/user/<int:uid>/group', methods=['GET'])
def get_usergroups(uid):
    args = request.args
    try: 
        with session_scope() as sess:
            query = sess.query(Group).filter(Group.join_relations.any(user_id=uid)).filter_by(is_public=1)
            groups = fetchobjs(query, args.get('page'), args.get('page_size'))
            return returnobjs(groups, 'groups', query.count())
    except AppError as e:
        return e.errdict

def get_object(obj_class, obj_id, obj_name, tstring = None):

    """ Get a single object by id (requires token if non-public) """
    
    try:
        with session_scope() as sess:
            obj = sess.query(obj_class).get(obj_id)
            if hasattr(obj, 'group_id'):
                resolve_permission(sess, obj.group_id, tstring, allow_public = True) # users are allowed to see it without having to be in the group
            
            fks = []
            if obj_name == 'feed':
                fks = ['author', 'group']
            if obj_name == 'group':
                fks = ['creator']
            return returnobj(obj, obj_name, fks = fks, get_files = True)
    except AppError as e:
        return e.errdict

@app.route('/feed/<int:fid>', methods=['GET'])
def get_feed(fid):
    return get_object(Feed, fid, 'feed', request.headers.get('Authorization'))

@app.route('/user/<int:uid>', methods=['GET'])
def get_user(uid):
    return get_object(User, uid, 'user', request.headers.get('Authorization'))

@app.route('/group/<int:gid>', methods=['GET'])
def get_group(gid):
    return get_object(Group, gid, 'group', request.headers.get('Authorization'))

@app.route('/organization/<int:oid>', methods=['GET'])
def get_org(oid):
    return get_object(Organization, oid, 'organization', request.headers.get('Authorization'))

@app.route('/feed/<int:pid>/comment', methods=['GET'])
def get_comments(pid):
    
    """ returns all of the comments under parent item / feed (requires token if non-public) """
    
    tstring = request.headers.get('Authorization')
    args = request.args
    try:
        with session_scope() as sess:
            parent = sess.query(Feed).get(pid)
            resolve_permission(sess, parent.group_id, tstring, allow_public = True)
            query = parent.comments
            comments = fetchobjs(query, args.get('page'), args.get('page_size'))
            return returnobjs(comments, 'comments', query.count(), fks = ['author'])
    except AppError as e:
        return e.errdict

# user update

@app.route('/user/update', methods=['PUT'])
def update_user():

    """ update user info """

    req = request.form
    tstring = request.headers.get('Authorization')

    try: 
        with session_scope() as sess:
            token = resolve_token(tstring)
            u = sess.query(User).get(token['user_id'])
            # about update
            u.about = req.get('about')
            # profile pic update
            profpic = request.files.get("file")
            key = u.profpic_key
            if profpic:
                ftype = profpic.filename.rsplit('.', 1)[1].lower()
                if not ('.' in profpic.filename and ftype in ALLOWED_UPLOAD_EXTENSIONS):
                    raise FileTypeError

                fname = secure_filename(f'profpic.{ftype}') # for security 
                key = f'users/{u.id}/profile/{fname}'
                S3.put_object(Bucket=BUCKET, Key=key, Body=profpic)
                u.profpic_key = key

            sess.commit()
            return {'status': 1, 'user': to_dict(u), 'profpic_key': key}

    except AppError as e:
        return e.errdict


@app.route('/group/<int:gid>/join', methods=['POST'])
def join_group(gid):

    tstring = request.headers.get('Authorization')
    try:
        with session_scope() as sess:
            token = resolve_token(tstring)

            if sess.query(JoinRelation).filter_by(group_id=gid, user_id=token['user_id']).scalar():
                return errmap[20] # already joined

            g = sess.query(Group).get(gid)
            if not g:
                raise GroupNotFoundError
            u = sess.query(User).get(token['user_id'])
            if g.org_id != u.org_id:
                return errmap[25]
            if g.is_public == 0:
                return errmap[19] # not allowed to join private group without invite
            
            newjoin = JoinRelation(group_id=gid, user_id=token['user_id'])
            sess.add(newjoin)

            sess.commit()
            return {'status': 1, 'action': 'join'}

    except AppError as e:
        return e.errdict

@app.route('/group/<int:gid>/leave', methods=['POST'])
def leave_group(gid):

    tstring = request.headers.get('Authorization')
    try:
        with session_scope() as sess:
            token = resolve_token(tstring)
            join = sess.query(JoinRelation).filter_by(group_id=gid, user_id=token['user_id']).first()
            if not join:
                return errmap[21]
            sess.delete(join)

            sess.commit()
            return {'status': 1, 'action': 'leave'}

    except AppError as e:
        return e.errdict

@app.route('/group/new', methods=['POST'])
def new_group():
    
    """ Create a new group & add the creator to the group """

    req = request.get_json()
    if not all([req, req.get('name'), req.get('about'), (req.get('is_public') == 0 or req.get('is_public') == 1)]):
        return errmap[4]
    
    try: 
        with session_scope() as sess:
            token = resolve_token(request.headers.get('Authorization'))
            oid = sess.query(User.org_id).filter_by(id=token['user_id']).first()

            newgroup = Group(name=req['name'], about=req['about'], user_id=token['user_id'], is_public=req['is_public'], org_id=oid)
            sess.add(newgroup)
            sess.flush()
            sess.add(JoinRelation(user_id=token['user_id'], group_id=newgroup.id))

            sess.commit()
            return {'status': 1, 'group': to_dict(newgroup)}

    except AppError as e:
        return e.errdict

@app.route('/group/<int:gid>/feed/new', methods=['POST'])
def new_feed(gid):

    """ add a new feed with token """

    req = request.form # form data 
    if not all([req, req.get('title'), req.get('body'), req.get('tag')]):
        return errmap[4]

    tstring = request.headers.get('Authorization')
    try:
        with session_scope() as sess:
            rdict = resolve_permission(sess, gid, tstring, allow_public = False)
            newfeed = Feed(tag=req['tag'], user_id=rdict['user'].id, group_id=gid, title=req['title'], body=req['body'], org_id=rdict['user'].org_id)
            sess.add(newfeed)
            sess.flush()

            # attachment(s)
            files = request.files.getlist('file_list')
            keys = []
            if files:
                # run checks
                for fp in files: 
                    ftype = fp.filename.rsplit('.', 1)[1].lower()
                    if not ('.' in fp.filename and ftype in ALLOWED_UPLOAD_EXTENSIONS):
                        raise FileTypeError

                for fp in files:
                    # config
                    fp.filename = secure_filename(fp.filename) # for security 
                    key = f'items/{newfeed.id}/{fp.filename}' 
                    S3.put_object(Bucket=BUCKET, Key=key, Body=fp)
                    keys.append(key)

                    # fp must be binary data
                    # put_object => upload binary

            newfeed.file_keys = keys 
            sess.commit()
            return returnobj(newfeed, 'feed')

    except AppError as e:
        return e.errdict


@app.route('/feed/<int:parent_id>/comment/new', methods=['POST'])
def new_comment(parent_id):

    """ add a new comment to a feed with token and parent_id (feed_id) """

    req = request.get_json()

    if not req.get('content'):
        return errmap[4]

    tstring = request.headers.get('Authorization')

    try:
        with session_scope() as sess:
            parent = sess.query(Feed).get(parent_id)
            if not parent:
                raise AssociationError

            rdict = resolve_permission(sess, parent.group_id, tstring, allow_public = False)
            
            # new comment logic
            parent.comment_count += 1
            newcomment = Comment(user_id=rdict['user'].id, content=req['content'], parent_id=parent_id, group_id=parent.group_id, org_id=parent.org_id)
            sess.add(newcomment)

            sess.commit()
            return {'status': 1, 'comment': to_dict(newcomment)}

    except AppError as e:
        return e.errdict

@app.route('/feed/<int:item_id>/like', methods=['POST'])
def like_item(item_id = None):
    tstring = request.headers.get('Authorization')
    try:
        with session_scope() as sess:
            # confirm existance
            item = sess.query(Item).get(item_id)
            if not item:
                raise errmap[16]
            token = resolve_token(tstring)

            like = sess.query(LikeRelation).filter_by(item_id=item_id, user_id=token["user_id"]).first()

            is_cancel = 0 # indicates whether the attempt cancels the last like
            if like:
                sess.delete(like)
                item.like_count -= 1
                is_cancel = 1
            else:
                sess.add(LikeRelation(item_id=item_id, user_id=token["user_id"]))
                item.like_count += 1
                is_cancel = 0

            sess.commit()
            return {'status': 1, 'is_cancel': is_cancel}

    except AppError as e:
        return e.errdict

@app.route('/feed/<int:item_id>/delete', methods=['DELETE'])
def delete_item(item_id = None):

    """ author removes one of his feeds """

    tstring = request.headers.get('Authorization')
    try:
        with session_scope() as sess:
            item = sess.query(Item).get(item_id)
            if not item: 
                raise errmap[16]
            
            # only the author of the item can delete it
            token = resolve_token(tstring)
            if token['user_id'] != item.user_id:
                raise errmap[17]

            # delete files attached
            if item.file_keys:
                s3_response = S3.delete_objects(Bucket=BUCKET, Delete={'Objects': [{'Key': fkey} for fkey in item.file_keys]})
            
            sess.delete(item)
            sess.commit()
            return {'status': 1}

    except AppError as e:
        return e.errdict

# user login / registration TODO not used for now

@app.route('/googlesignin', methods=['POST'])
def googlesignin():

    """ login with google, create a new google user if email not taken """

    req = request.get_json()
    if not req.get('idtoken'):
        return errmap[4]

    try:
        with session_scope() as sess:
            guser = id_token.verify_oauth2_token(req['idtoken'], requests.Request(), CLIENT_ID)

            if guser['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')

            u = sess.query(User).filter_by(email=guser['email']).first()
            # no user
            if not u:
                udomain = guser['email'].split('@')[1]
                org = sess.query(Organization).filter_by(domain=udomain, status=1).first()
                # no org
                if not org:
                    return errmap[8]
                u = User(firstname=guser['given_name'], lastname=guser['family_name'], email=guser['email'], org_id=org.id) 
                sess.add(u)
                sess.flush()

            payload = {'user_id': u.id, 'exp': int(time.time()) + 24*60*60} # 24hs
            token = jwt.encode(payload, PRIVATE, algorithm='RS256')
            sess.commit()
            return {'status': 1, 'token': token.decode('utf-8'), 'user': to_dict(u)}

    except AppError as e:
        return e.errdict


# Sign in and sign up are disabled.
#@app.route('/signin', methods=['POST'])
def signin():

    """ sign in with email and pass """

    req = request.get_json()
    if not all([req, req.get('email'), req.get('password')]):
        return errmap[4]
    
    try:
        with session_scope() as sess:
            u = sess.query(User).filter_by(email=req['email']).first()
            
            if not u:
                return errmap[2]

            # verify pass
            if(argon2.verify(req['password'], u.password)):
                payload = {'user_id': u.id, 'exp': int(time.time()) + 60*60}
                token = jwt.encode(payload, PRIVATE, algorithm='RS256')
                return {'status': 1, 'token': token.decode('utf-8'), 'user': to_dict(u)}
            else:
                return errmap[3]
    
    except AppError as e:
        return e.errdict


#@app.route('/signup', methods=['POST'])
def signup():

    """ signup a user """

    req = request.get_json()
    
    if not all([req, req.get('firstname'), req.get('lastname'), req.get('email'), req.get('password')]):
        return errmap[4]
    
    try:
        with session_scope() as sess:
            if (sess.query(User).filter_by(email=req['email']).scalar()):
                return errmap[7] # email taken

            newuser = User(firstname=req['firstname'], lastname=req['lastname'], email=req['email'], password=argon2.hash(req['password']), org_id=1)
            sess.add(newuser)
        
            sess.commit()
            return returnobj(newuser, 'user')
    except AppError as e:
        return e.errdict


@app.route("/group/<int:gid>/invitation/send", methods=['POST'])
def send_invite(gid):
    req = request.get_json()
    scope = request.args.get("scope")
    tstring = request.headers.get('Authorization')

    if not all([(req.get('email_list') or scope == "anyone"), scope, scope in ["anyone", "listed"]]):
        return errmap[4]

    try:
        with session_scope() as sess:
            rdict = resolve_permission(sess, gid, tstring, allow_public = False)
            
            g = rdict['group']
            invitor = rdict['user']
            org_domain = g.organization.domain
            email_list = req.get('email_list')
            
            # email domain mismatch
            for email in email_list:
                domain = email.split('@')[1]
                if domain != org_domain:
                    return errmap[26]
                
            digest = md5(str(randint(1,1<<31)).encode()).hexdigest()
            url = f'?group={g.name}&redirect=/group/{g.id}?invite_code='
            share_link = f'https://{ROOT}{url}{digest}'
            #share_link = f'http://localhost:3000{url}{digest}' # testing
            
            sess.add(InviteCode(id=digest, invitor_id=invitor.id, invitee_emails=req.get('email_list'), group_id=g.id, scope=scope))

            #below is for future email service
            subject = f'{invitor.firstname} is inviting you to join {g.name}'
            details = {"invitor_name": invitor.firstname, 'group_name': g.name, 'share_link': share_link}
            for email in req['email_list']:
                msg = create_message("support@groupible.com", email, subject, emailmsg(details, "invite"))
                #msg = create_message("zachzhu2016@gmail.com", email, subject, emailmsg(details, "invite"))
                send_message(service, "me", msg)
            
            sess.commit()
            return {"status": 1, "share_link": share_link}

    except AppError as e:
        return e.errdict

@app.route("/group/<int:gid>/invitation/accept", methods=["GET"])
def accept_invite(gid):
    code = request.args.get("invite_code")
    tstring = request.headers.get('Authorization')
    if not code:
        return errmap[4]
    try:
        with session_scope() as sess:
            inv = sess.query(InviteCode).get(code)
            token = resolve_token(tstring)
            invitee = sess.query(User).get(token['user_id'])
            g = sess.query(Group).get(gid)
            
            if invitee.org_id != g.org_id:
                return errmap[25]

            if inv.scope == 'listed' and invitee.email not in inv.invitee_emails:
                return errmap[23] # not on the list
            if ((datetime.now(timezone.utc) - inv.created).days) > 0:
                sess.delete(inv) # link expired (1 day)
                sess.commit()
                return errmap[24]

            join = sess.query(JoinRelation).filter_by(user_id=invitee.id, group_id=inv.group_id).first()
            if join:
                return errmap[20]

            sess.add(JoinRelation(user_id=invitee.id, group_id=inv.group_id))
            sess.commit()
            return {"status": 1}

    except AppError as e:
        return e.errdict

@app.route("/finder", methods=["GET"])
def finder():
    """ 
    allowed query param combinations:
        The state between user and group: func=relation&groupId=<int> 
    """

    func = request.args.get("func") 
    gid = request.args.get("groupId") 
    tstring = request.headers.get('Authorization')

    try:
        with session_scope() as sess:
            # checks if a relation exists between current user and a group
            if func and func == "relation" and gid:
                if tstring == 'null': 
                    return {'status': 1, 'relation': 'none'} # user not logged in 

                token = resolve_token(tstring)
                user = sess.query(User).get(token['user_id'])
                if not user:
                    return errmap[6]  
                
                if int(gid) in {r.group_id for r in user.join_relations}:
                    return {"status": 1, "relation": "in"}
                return {"status": 1, "relation": "out"}
            else:
                return errmap[4]
    except AppError as e:
        return e.errdict


# helpers 

@app.route('/search', methods=['GET'])
def search():
    args = request.args
    obj_name = args.get('objName')
    q = args.get('q')
    if not all([obj_name, q]):
        return errmap[4]
    try:
        with session_scope() as sess:
            qdict = {'feed': (sess.query(Feed).filter(Feed.title.like(f'%{q}%')), "feeds"),
                     'user': (sess.query(User).filter(User.firstname.like(f'%{q}%') | User.lastname.like(f'%{q}%')), "users"),
                     'group': (sess.query(Group).filter(Group.name.like(f'%{q}%')), "groups")
                    }
            if not qdict.get(obj_name):
                raise
            query, rkey = qdict.get(obj_name)
            objs = fetchobjs(query, args.get('page'), args.get('page_size'))
            fks = []
            if rkey == 'feeds':
                fks = ['author', 'group']
            if rkey == 'groups':
                fks = ['creator']
            return returnobjs(objs, rkey, query.count(), fks = fks)
    except AppError as e:
        return e.errdict
        
    
def fetchobjs(query, page, page_size):
    #defaulr values 
    page = int(page or 0)
    page_size = int(page_size or 16)
    offset = page * page_size
    return query.all()[offset:offset + page_size]

def joinobj(obj, fks) -> dict:
    addon = dict()
    for fk in fks:
        addon[fk] = to_dict(getattr(obj, fk))
    return addon

def make_urls(keys):
    presigned_urls = []
    if not keys: 
        return presigned_urls
    for key in keys:
        url = S3.generate_presigned_url('get_object', Params={'Bucket': BUCKET, 'Key': key})
        presigned_urls.append(url)
    return presigned_urls


def returnobj(obj: Base, name: str, exclude = set(), fks = list(), get_files = False):
    
    """ return an object from db """
    
    if not obj: 
        return errmap[16]

    # generate presigned urls for s3 access
    if get_files and hasattr(obj, "file_keys"):
        setattr(obj, "file_keys", make_urls(getattr(obj, "file_keys")))

    try:
        if fks:
            return {'status': 1, name: {**to_dict(obj, exclude), **joinobj(obj, fks)}}
        return {'status': 1, name: to_dict(obj, exclude)}
    except Exception as e:
        app.logger.info('%s', e)
        return errmap[5]


def returnobjs(objlist: iter, name: str, total: int, exclude = set(), fks = list(), get_files = False):
    
    """ return a list of objects from db """

    if not objlist:
        return {'status': 1, name: []}

    # generate presigned urls for s3 access
    if get_files and hasattr(objlist[0], "file_keys"):
        for obj in objlist:
            setattr(obj, "file_keys", make_urls(getattr(obj, "file_keys")))

    try:
        if fks:
            return {'status': 1, 'total': total,  name: [{**to_dict(obj), **joinobj(obj, fks)} for obj in objlist]}
        return {'status': 1, 'total': total, name: [to_dict(obj, exclude) for obj in objlist]}
    except Exception as e:
        app.logger.info('%s', e)
        return errmap[5]

def resolve_token(tstring):
    if tstring == 'null': # special case 
        raise TokenNotFoundError # user not signed in
    return jwt.decode(tstring, PUBLIC, algorithm='RS256')

def resolve_permission(sess, gid, tstring = None, allow_public = False):
    g = sess.query(Group).filter_by(id=gid, status=1).first()
    u = None
    if not g:
        raise GroupNotFoundError
    if g.is_public == 0 or not allow_public:
        token = resolve_token(tstring)
        u = sess.query(User).get(token['user_id'])
        if not (sess.query(JoinRelation).filter_by(user_id=token['user_id'], group_id=gid).scalar() \
                and \
                u.org_id == g.org_id):
            raise UserPermissionError
    return {"group": g, "user": u if u else None}

@contextmanager
def session_scope():

    """ Provide a transactional scope around a series of operations. """
    
    session = Session()
    try:
        yield session
    except TokenNotFoundError:
        raise AppError(errmap[15])
    except InvalidTokenError:
        raise AppError(errmap[10])
    except AssociationError:
        raise AppError(errmap[18])
    except GroupNotFoundError:
        raise AppError(errmap[22])
    except UserPermissionError:
        raise AppError(errmap[17])
    except FileTypeError:
        session.rollback()
        raise AppError(errmap[14])
    except FileSizeError:
        session.rollback()
        raise AppError(errmap[27])
    except ClientError as e:
        app.logger.info('%s', e)
        session.rollback()
        raise AppError(errmap[28])
    except ValueError:
        session.rollback()
        raise AppError(errmap[11])
    except GmailError:
        session.rollback()
        raise AppError(errmap[29])
    except Exception as e:
        app.logger.info('%s', e)
        session.rollback()
        raise AppError(errmap[5])
    finally:
        session.close()

# main 
if __name__ == '__main__':
    try:
        username,password,host,port,database = sys.argv[1:]
    except:
        raise Exception
    engine = create_engine(f"postgresql+psycopg2://{username}:{password}@{host}:{port}/{database}", pool_pre_ping=True)
    Session = sessionmaker(bind=engine)

    # keys
    PRIVATE = open("private.pem", "r").read() 
    PUBLIC = open("public.pem", "r").read()
    
    # google 
    with open("google_signin_credentials.json", "r") as fp:
        google_cred = json.load(fp)
        CLIENT_ID = google_cred['web']['client_id']
    
    # aws S3 
    S3 = boto3.client('s3')
    BUCKET = 'groupible'
    ALLOWED_UPLOAD_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}
    ALLOWED_PROFPIC_EXTENSIONS = {'png', 'jpg', 'jpeg'}

    # service 
    service = mailmain()

    app.run(host='0.0.0.0', port=80, debug=True)

